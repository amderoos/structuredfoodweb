/***
   NAME
     UniDev-NumRec.h
   PURPOSE
     Routine is adapted from the routine ran1() in "Numerical Recipes in C". 
     It returns a uniform random deviate between 0.0 and 1.0 and is entirely 
     portable.
     
   Last modification: AMdR - Feb 26, 2020
***/

#include "sys/time.h"
#include "time.h"

#define MAX_STORED                100

#define M1                        259200
#define IA1                       7141
#define IC1                       54773
#define RM1                       (1.0/M1)
#define M2                        134456
#define IA2                       8121
#define IC2                       28411
#define RM2                       (1.0/M2)
#define M3                        243000
#define IA3                       4561
#define IC3                       51349

double UniDev(void)

{
  static long       ix1, ix2, ix3;
  static double     prevrans[MAX_STORED];
  static int        iff = 0;
  int               j;
  double            randev;
  struct timeval    tptr;
  struct timezone   tzptr;

  if (iff == 0)
    {
      // Initialization of the random number generator
      if (!UniDevSeed)
        {
          gettimeofday(&tptr, &tzptr);
          UniDevSeed = tptr.tv_usec;
        }

      ix1 = (IC1 - UniDevSeed) % M1;
      ix1 = (IA1*ix1 + IC1) % M1;
      ix2 = ix1 % M2;
      ix1 = (IA1*ix1 + IC1) % M1;
      ix3 = ix1 % M3;
      for (j = 0; j < MAX_STORED; j++)
        {
          ix1         = (IA1*ix1 + IC1) % M1;
          ix2         = (IA2*ix2 + IC2) % M2;
          prevrans[j] = (ix1 + ix2*RM2)*RM1;
        }
      iff = 1;
    }

  ix1 = (IA1*ix1 + IC1) % M1;
  ix2 = (IA2*ix2 + IC2) % M2;
  ix3 = (IA3*ix3 + IC3) % M3;
  j   = ((MAX_STORED*ix3)/(double)M3);

  if ((j < 0) || (j >= MAX_STORED)) ProgramExit(1, "UniDev: Invalid index.");

  randev      = prevrans[j];
  prevrans[j] = (ix1 + ix2*RM2)*RM1;

  return randev;
}

#undef M1
#undef IA1
#undef IC1
#undef RM1
#undef M2
#undef IA2
#undef IC2
#undef RM2
#undef M3
#undef IA3
#undef IC3

//==================================================================================
