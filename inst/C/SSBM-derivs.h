/***
   NAME
     SSBM-derivs.h
   PURPOSE
     Derivatives routine for the 1-, 2- or 3-stage Stage-Structured Biomass Model foodweb model
     
   Last modification: AMdR - Feb 23, 2020
***/

#if (STAGES > 1)
double                            JuvTotMort[MAX_SPECIES];
#if (STAGES > 2)
double                            ImmTotMort[MAX_SPECIES];
#endif
#endif

#define MAX_EXP 50.0

double Maturation(double logz, double nuj, double muj)

{
  double tmp, tres, matrate = 0.0;

  tres = muj / (1.0 - MAX_EXP / logz);
  if (nuj < tres)
    matrate = 0.0;
  else
    {
      tmp = 1.0 - muj / nuj;
      if (fabs(tmp) < 1.0E-6)
        matrate = tmp / 2 - 1 / logz;
      else
        matrate = tmp / (1.0 - exp(tmp * logz));
    }
  matrate *= nuj;

  return matrate;
}

//==================================================================================

void Gradient(double time, double *y, double *dydt)

{
  register int                    ii, prey, pred;
  double                          resource, predation;
  double                          adunet, reproduction;
  register double                 *adudens, *dadudensdt;
#if (STAGES > 1)
  double                          juvnet, juvmat;
  register double                 *juvdens, *djuvdensdt;
  double                          logz = log(JUVADUSIZERATIO);

  juvdens    = y;
  djuvdensdt = dydt;

#if (STAGES > 2)
  double                          immnet, immmat;
  register double                 *immdens, *dimmdensdt;

  immdens    = y + Species;
  dimmdensdt = dydt + Species;
#endif
#endif
  adudens    = y + (STAGES - 1) * Species;
  dadudensdt = dydt + (STAGES - 1) * Species;

  for (pred = 0; pred < Species; pred++)
    {
      dadudensdt[pred] = -(2.0 - QMORT) * Mort[pred] * dmax(adudens[pred], 0);      // Background adult mortality

      if (adudens[pred] <= MACH_PREC) adudens[pred] = 0.0;
#if (STAGES > 1)
      djuvdensdt[pred] = -(QMORT) * Mort[pred] * dmax(juvdens[pred], 0);            // Background juvenile mortality
      JuvTotMort[pred] =  (QMORT) * Mort[pred];

      if (juvdens[pred] <= MACH_PREC) juvdens[pred] = 0.0;
#if (STAGES > 2)
      dimmdensdt[pred] = -(2.0 - QMORT) * Mort[pred] * dmax(immdens[pred], 0);      // Background immature mortality
      ImmTotMort[pred] =  (2.0 - QMORT) * Mort[pred];

      if (immdens[pred] <= MACH_PREC) immdens[pred] = 0.0;
#endif
#endif
    }

  // Set derivatives for all species
  // Start at the species with the highest index, which is the top-most consumer
  // When derivatives of a species are being computed, predation mortality is alreayd known
  for (pred = Species - 1; pred >= 0; pred--)
    {
      predation = 0.0;

#if (STAGES == 1)
      if (adudens[pred] <= MACH_PREC) continue;
#else
#if (STAGES == 2)
      if ((juvdens[pred] <= MACH_PREC) && (adudens[pred] <= MACH_PREC)) continue;

      predation        += (QALPHA) * Alpha[pred] * juvdens[pred];
#else
      if ((juvdens[pred] <= MACH_PREC) && (immdens[pred] <= MACH_PREC) && (adudens[pred] <= MACH_PREC)) continue;

      predation        += (2.0 - QALPHA) * Alpha[pred] * juvdens[pred];
      predation        += (QALPHA) * Alpha[pred] * immdens[pred];
#endif
#endif
      predation        += (2.0 - QALPHA) * Alpha[pred] * adudens[pred];

      // First compute resource availability
      for (ii = 0, resource = 0; ii < PreyNr[pred]; ii++)
        {
          prey = prey_index[pred][ii];

#if (STAGES == 1)
          if (adudens[prey] <= MACH_PREC) continue;
#else
#if (STAGES == 2)
          if ((juvdens[prey] <= MACH_PREC) && (adudens[prey] <= MACH_PREC)) continue;
#else
          if ((juvdens[prey] <= MACH_PREC) && (immdens[prey] <= MACH_PREC) && (adudens[prey] <= MACH_PREC)) continue;
          resource += (2.0 - JuvAduSpec[pred]) * Phi[pred][prey] * immdens[prey];
#endif
          resource += (JuvAduSpec[pred]) * Phi[pred][prey] * juvdens[prey];
#endif
          resource += (2.0 - JuvAduSpec[pred]) * Phi[pred][prey] * adudens[prey];
        }

      if (pred < BASALSPECIES)
        {
          resource = PRODUCTIVITY / (TURNOVER + predation);
          // If basal species do not compete divide total productivity by the number of
          // basal species to keep total system productivity the same
          resource /= BASALSPECIES;
        }
      else
        {
          predation /= (Fhres[pred] + resource);                                    // Ajust predation before resource
          resource  /= (Fhres[pred] + resource);
          for (ii = 0; ii < PreyNr[pred]; ii++)
            {
              prey = prey_index[pred][ii];
#if (STAGES == 1)
              if (adudens[prey] <= MACH_PREC) continue;
#else
#if (STAGES == 2)
              if ((juvdens[prey] <= MACH_PREC) && (adudens[prey] <= MACH_PREC)) continue;
#else
              if ((juvdens[prey] <= MACH_PREC) && (immdens[prey] <= MACH_PREC) && (adudens[prey] <= MACH_PREC)) continue;

              dimmdensdt[prey] -= (2.0 - JuvAduSpec[pred]) * Phi[pred][prey] * predation * immdens[prey];
              ImmTotMort[prey] += (2.0 - JuvAduSpec[pred]) * Phi[pred][prey] * predation;
#endif
              djuvdensdt[prey] -= JuvAduSpec[pred] * Phi[pred][prey] * predation * juvdens[prey];
              JuvTotMort[prey] += JuvAduSpec[pred] * Phi[pred][prey] * predation;
#endif
              dadudensdt[prey] -= (2.0 - JuvAduSpec[pred]) * Phi[pred][prey] * predation * adudens[prey];
            }
        }

      adunet       = (2.0 - QALPHA) * Gamma[pred] * resource - (2.0 - QMAINT) * Maint[pred];
      reproduction = dmax(adunet, 0) * adudens[pred];

      dadudensdt[pred] += dmin(adunet, 0) * adudens[pred];                          // Starvation mortality of adults
#if (STAGES == 1)
      dadudensdt[pred] += reproduction;
#elif (STAGES == 2)
      juvnet = (QALPHA) * Gamma[pred] * resource - (QMAINT) * Maint[pred];

      juvmat = Maturation(logz, juvnet, JuvTotMort[pred]) * juvdens[pred];
      djuvdensdt[pred] += juvnet * juvdens[pred];                                   // Somatic growth and starvation mortality of juveniles

      djuvdensdt[pred] += reproduction - juvmat;
      dadudensdt[pred] += juvmat;
#else
      juvnet = adunet;
      immnet = (QALPHA) * Gamma[pred] * resource - (QMAINT) * Maint[pred];

      juvmat = Maturation(logz, juvnet, JuvTotMort[pred]) * juvdens[pred];
      immmat = Maturation(logz, immnet, ImmTotMort[pred]) * immdens[pred];

      djuvdensdt[pred] += juvnet * juvdens[pred];                                   // Somatic growth and starvation mortality of juveniles
      djuvdensdt[pred] += reproduction - juvmat;

      dimmdensdt[pred] += immnet * immdens[pred];                                   // Somatic growth and starvation mortality of immatures
      dimmdensdt[pred] += juvmat - immmat;

      dadudensdt[pred] += immmat;
#endif
    }

  return;
}

//==================================================================================
