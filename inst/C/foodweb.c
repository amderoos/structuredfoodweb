/***
   NAME
     foodweb.c
   PURPOSE
     Program simulates a food web consisting of a variable number of basal species
     and a variable number of consumer species with or without stage-structure.
     
     Dynamics are modeled with either the stage-structured LV model with maintenance
     or with the stage-structured biomass model. In case of the stage-structured 
     LV model with maintenance either juvenile-adult stage structure or no 
     stage-structure is assumed. The stage-structured biomass model can be simulated
     with 1, 2 or 3 stages.

     Juvenile and adult individuals are assumed to have completely identical
     niches, ii.e. they feed on the same prey species, irrespective of prey
     stage. This is to avoid ontogenetic niche shifts to occur.
     Ontogenetic asymmetry hence arises only through differences in the
     juvenile-adult ratio of ingestion, production, maintenance and mortality.

     All basal species are assumed to forage on their own exclusive resource. 
     The dynamics of this shared resoource follows:

     dR/dt = Productivity - Turnover*R - <Foraging pressure by basal species N>*R

     This exclusive resource is assumed to be in pseudo-steady-state such that the
     resource availability for basal species N equals

     R =  Productivity/(Turnover + <Foraging by basal species N>)

     Non-basal species are assumed to forage on all species in their niche, following
     a type II functional response.

   Last modification: AMdR - Nov 03, 2020
***/

#define FOODWEB_C
#include "foodweb.h"

#ifndef SSBM
#define SSBM                      0
#endif

#if (SSBM == 0)
#if ((STAGES != 1) && (STAGES != 2))
#error With SSBM equal to 0 STAGES should be 1 or 2
#endif
#if (JFRAC == 1)
#include "LV-JFrac-derivs.h"
#else
#define ADJUSTJAFRAC              0                                                 // Do / don't account for J/A ratio
#include "LV-derivs.h"
#endif
#else
#undef SSBM
#define SSBM                      1
#if ((STAGES != 1) && (STAGES != 2) && (STAGES != 3))
#error With SSBM equal to 1 STAGES should be 1, 2 or 3
#endif
#include "SSBM-derivs.h"
#endif

//==================================================================================
/*
 * Start of function implementations.
 */
//==================================================================================

static void UpdateVars(double age)

{
  int                             jj;

#if (JFRAC == 1)
#if (JFRACFIXED == 1)
  double                          *juvfrac = SpeciesDblData + (SPECIESDBLLEN - 1) * Species;
#else
  double                          *juvfrac = SpeciesDblData + (SPECIESDBLLEN - 3) * Species;
#endif
  double                          *totdens = SpeciesDblData + (SPECIESDBLLEN - 2) * Species;

  for (jj = 0; jj < Species; jj++)
    {
      JuvDens[jj] =      juvfrac[jj]  * totdens[jj];
      AduDens[jj] = (1 - juvfrac[jj]) * totdens[jj];
    }
#endif

  for (jj = 0; jj < Species; jj++)
    {
      // Reset Total species density
      AduDens[jj] = dmax(AduDens[jj], MACH_PREC);
#if (STAGES == 1)
      TotDens[jj] = AduDens[jj];
#elif (STAGES == 2)
#if (QNA == 1)
      JuvDens[jj] = ReproVal[jj] * AduDens[jj];
#endif
      JuvDens[jj] = dmax(JuvDens[jj], MACH_PREC);
      TotDens[jj] = JuvDens[jj] + AduDens[jj];
#else
      JuvDens[jj] = dmax(JuvDens[jj], MACH_PREC);
      ImmDens[jj] = dmax(ImmDens[jj], MACH_PREC);
      TotDens[jj] = JuvDens[jj] + ImmDens[jj] + AduDens[jj];
#endif
      SpeciesAge[jj] = age;
    }

  return;
}

//==================================================================================

static int CheckExtinctions()

{
  int                             ii, jj, newSpecies, Extinction = 0;
  static FILE                     *extfil = NULL;
  static long                     Observations = 0L;
  static int                      first = 1;
  int                             NoStatsChanged;
  double                          deviation, lastavg, lastvar, lastmax, lastmin, relchange;
  static double                   LastExtInvTime;
  static double                   LastStatsChangeTime;

  for (jj = BASALSPECIES, newSpecies = BASALSPECIES; jj < Species; jj++)
    {
      // After transient time has expired remove non-basal species below the extinction threshold
      if (TotDens[jj] <= EXTINCT)
        {
          Extinction++;
          TotDens[jj] = AduDens[jj] = 0.0;
#if (STAGES > 1)
          JuvDens[jj] = 0.0;
#if (STAGES > 2)
          ImmDens[jj] = 0.0;
#endif
#endif
#if (JFRAC == 1)
          SpeciesDblData[(SPECIESDBLLEN - 2) * Species + jj] = 0.0;
#endif
          if (!extfil)
            {
              char                filename[MAXPATHLEN];

              strcpy(filename, runname);
              strcat(filename, ".web");
              extfil = fopen(filename, "w");
            }
          fprintf(extfil, "EXTINCTION\tTime:%12.1f\tSpecies: %5d\tBody size: %15.4G\tAge: %12.1f\n",
                  CurrentTime, SpeciesID[jj], SpeciesBodySize[jj], SpeciesAge[jj]);
          fflush(extfil);
          continue;
        }
      if (newSpecies != jj)                                                         // In-between species have been removed: copy
        {
          for (ii = 0; ii < SPECIESDBLLEN; ii++) 
            SpeciesDblData[ii * Species + newSpecies] = SpeciesDblData[ii * Species + jj];
          for (ii = 0; ii < SPECIESINTLEN; ii++) 
            SpeciesIntData[ii * Species + newSpecies] = SpeciesIntData[ii * Species + jj];
        }
      newSpecies++;
    }

  if (newSpecies != Species)
    {
      Species = RearrangeMem(Species, newSpecies);
      ConstructInteractionMatrix();
    }

  if (Extinction || first)
    {
      fflush(extfil);

      // Re-initialize the maxima and minima
      memset(TotDensMean,     0, Species * sizeof(double));
      memset(TotDensVariance, 0, Species * sizeof(double));
      memset(TotDensMax,      0, Species * sizeof(double));
      for (ii = 0; ii < Species; ii++) TotDensMin[ii] = 0.1*DBL_MAX;

      memset(AduDensMean,     0, Species * sizeof(double));
      memset(AduDensVariance, 0, Species * sizeof(double));
      memset(AduDensMax,      0, Species * sizeof(double));
      for (ii = 0; ii < Species; ii++) AduDensMin[ii] = 0.1*DBL_MAX;

#if (STAGES > 1)
      memset(JuvDensMean,     0, Species * sizeof(double));
      memset(JuvDensVariance, 0, Species * sizeof(double));
      memset(JuvDensMax,      0, Species * sizeof(double));
      for (ii = 0; ii < Species; ii++) JuvDensMin[ii] = 0.1*DBL_MAX;
#if (STAGES > 2)
      memset(ImmDensMean,     0, Species * sizeof(double));
      memset(ImmDensVariance, 0, Species * sizeof(double));
      memset(ImmDensMax,      0, Species * sizeof(double));
      for (ii = 0; ii < Species; ii++) ImmDensMin[ii] = 0.1*DBL_MAX;
#endif
#endif

      Observations        = 0L;
      LastExtInvTime      = CurrentTime;
      LastStatsChangeTime = CurrentTime;
    }
  first = 0;

  // Measure species densities if extinctions have not occurred in the last TRANSIENT_TIME time units
  if ((CurrentTime - LastExtInvTime) >= TRANSIENT_TIME)
    {
      NoStatsChanged = 0;
      Observations++;
      for (ii = 0; ii < Species; ii++)
        {
          lastmax = TotDensMax[ii];
          lastmin = TotDensMin[ii];
          lastavg = TotDensMean[ii];
          lastvar = TotDensVariance[ii];

          TotDensMax[ii] = dmax(TotDens[ii], TotDensMax[ii]);
          TotDensMin[ii] = dmin(TotDens[ii], TotDensMin[ii]);

          // Compute arithmetic mean
          deviation = TotDens[ii] - TotDensMean[ii];
          TotDensMean[ii] += (deviation/Observations);

          // Compute the variance
          if (Observations > 2) TotDensVariance[ii] *= (double)(Observations - 2);
          TotDensVariance[ii] += (deviation*(TotDens[ii] - TotDensMean[ii]));
          if (Observations > 1) TotDensVariance[ii] /= (double)(Observations - 1);

          // Checking of stationarity of the time series is carried out on total densities only
          if (Observations > 2)
            {
              relchange = 2.0*(TotDensMean[ii] - lastavg)/(TotDensMean[ii] + lastavg);
              if (!ii) NoStatsChanged = iszero(relchange);
              else
                NoStatsChanged = NoStatsChanged && iszero(relchange);

              relchange      = 2.0*(TotDensMax[ii] - lastmax)/(TotDensMax[ii] + lastmax);
              NoStatsChanged = NoStatsChanged && iszero(relchange);

              // Minimum density and variance may be (close to) 0, compare absolute change
              NoStatsChanged = NoStatsChanged && iszero((TotDensMin[ii] - lastmin));
              NoStatsChanged = NoStatsChanged && iszero((TotDensVariance[ii] - lastvar));
            }

#if (STAGES > 1)
          JuvDensMax[ii] = dmax(JuvDens[ii], JuvDensMax[ii]);
          JuvDensMin[ii] = dmin(JuvDens[ii], JuvDensMin[ii]);

          // Compute arithmetic mean
          deviation = JuvDens[ii] - JuvDensMean[ii];
          JuvDensMean[ii] += (deviation/Observations);

          // Compute the variance
          if (Observations > 2) JuvDensVariance[ii] *= (double)(Observations - 2);
          JuvDensVariance[ii] += (deviation*(JuvDens[ii] - JuvDensMean[ii]));
          if (Observations > 1) JuvDensVariance[ii] /= (double)(Observations - 1);
#if (STAGES > 2)
          ImmDensMax[ii] = dmax(ImmDens[ii], ImmDensMax[ii]);
          ImmDensMin[ii] = dmin(ImmDens[ii], ImmDensMin[ii]);

          // Compute arithmetic mean
          deviation = ImmDens[ii] - ImmDensMean[ii];
          ImmDensMean[ii] += (deviation/Observations);

          // Compute the variance
          if (Observations > 2) ImmDensVariance[ii] *= (double)(Observations - 2);
          ImmDensVariance[ii] += (deviation*(ImmDens[ii] - ImmDensMean[ii]));
          if (Observations > 1) ImmDensVariance[ii] /= (double)(Observations - 1);
#endif
#endif
          AduDensMax[ii] = dmax(AduDens[ii], AduDensMax[ii]);
          AduDensMin[ii] = dmin(AduDens[ii], AduDensMin[ii]);

          // Compute arithmetic mean
          deviation = AduDens[ii] - AduDensMean[ii];
          AduDensMean[ii] += (deviation/Observations);

          // Compute the variance
          if (Observations > 2) AduDensVariance[ii] *= (double)(Observations - 2);
          AduDensVariance[ii] += (deviation*(AduDens[ii] - AduDensMean[ii]));
          if (Observations > 1) AduDensVariance[ii] /= (double)(Observations - 1);
        }
      if (!NoStatsChanged) LastStatsChangeTime = CurrentTime;
      if ((CurrentTime - LastStatsChangeTime) > MIN_PERSIST_TIME) return 1;
      if (Species == BASALSPECIES) return 1;                                        // Stop if only basal species left
    }

  return 0;
}

//==================================================================================

static void DynamicOutput()

{
  int                             ii, jj;
  static int                      first = 1;
  char                            filename[MAXPATHLEN];
  static FILE                     *dynfil = NULL;

  if (first)
    {
      strcpy(filename, runname);                                                    // Open additional file for all species output
      strcat(filename, ".csv");
      dynfil = fopen(filename, "w");
    }

  PrettyPrint(dynfil, CurrentTime);

  for (ii = 0, jj = 0; ii < InitialSpecies; ii++)
    {
      fprintf(dynfil, ", ");
      if (ii == SpeciesCol[jj])
        PrettyPrint(dynfil, TotDens[jj++]);
      else
        PrettyPrint(dynfil, 0.0);
    }
#if (STAGES > 1)
  for (ii = 0, jj = 0; ii < InitialSpecies; ii++)
    {
      fprintf(dynfil, ", ");
      if (ii == SpeciesCol[jj])
        PrettyPrint(dynfil, JuvDens[jj++]);
      else
        PrettyPrint(dynfil, 0.0);
    }
#if (STAGES > 2)
  for (ii = 0, jj = 0; ii < InitialSpecies; ii++)
    {
      fprintf(dynfil, ", ");
      if (ii == SpeciesCol[jj])
        PrettyPrint(dynfil, ImmDens[jj++]);
      else
        PrettyPrint(dynfil, 0.0);
    }
#endif
#endif
  for (ii = 0, jj = 0; ii < InitialSpecies; ii++)
    {
      fprintf(dynfil, ", ");
      if (ii == SpeciesCol[jj])
        PrettyPrint(dynfil, AduDens[jj++]);
      else
        PrettyPrint(dynfil, 0.0);
    }
  fprintf(dynfil, "\n");
  fflush(dynfil);

  first = 0;

  return;
}

//==================================================================================

static void SummaryOutput()

{
  int                             ii, jj, prey, pred;
  int                             FinalLinks, FinalBU, FinalTD;
  double                          amplR, amplC;
  char                            filename[MAXPATHLEN];
  FILE                            *sumfil;
  struct stat                     st;

  strcpy(filename, runname);                                                        // Open additional file for summary output
  strcat(filename, ".avg");
  if ((stat(filename, &st) != 0) || (st.st_size == 0))
    {
      sumfil = fopen(filename, "w");                                                // New AVG file
      fprintf(sumfil, "#%10s %5s %7s %7s %8s %5s %7s %6s %7s %9s %9s\n",
              "Seed", "S(0)", "Csp(0)", "Cst(0)", "T", "S(T)", "Csp(T)", "Cst1(T)", "Cst2(T)", "R+/R-", "C+/C-");
      fflush(sumfil);
    }
  else
    sumfil = fopen(filename, "a");                                                  // Existing AVG file

  for (ii = 0, FinalBU = FinalTD = FinalLinks = 0; ii < Species; ii++)
    {
      for (jj = 0; jj < Species; jj++)
        {
          if (Phi[ii][jj] > MACH_PREC)
            FinalLinks++;
        }

      FinalBU += PreyNr[ii];
      FinalTD += PredNr[ii];
    }
  amplR = amplC = 0.0;
  prey = pred = 0;
  for (ii = 0; ii < BASALSPECIES; ii++)
    {
      if (!TotDensMin[ii]) continue;                                                // Should never be true

      amplR += TotDensMax[ii] / TotDensMin[ii];
      prey++;
    }
  if (prey) amplR /= prey;

  for (ii = BASALSPECIES; ii < Species; ii++)
    {
      if (!TotDensMin[ii]) continue;                                                // Should never be true

      amplC += TotDensMax[ii] / TotDensMin[ii];
      pred++;
    }
  if (pred) amplC /= pred;

  fprintf(sumfil, "%11ld %5d %7.4f %7.4f %8.0f %5d %7.4f %7.4f %7.4f %9.2G %9.2G\n", UniDevSeed, InitialSpecies,
          InitialLinks / ((double)(InitialSpecies * InitialSpecies)),
          InitialBU / ((double)(InitialSpecies * InitialSpecies)), CurrentTime,
          Species, FinalLinks / ((double)(Species * Species)), FinalBU / ((double)(Species * Species)),
          FinalTD / ((double)(Species * Species)), amplR, amplC);
  fflush(sumfil);
  fclose(sumfil);

  WebToCsbFile();

  return;
}

//==================================================================================

int main(int argc, char **argv)

{
  int                             ii, jj;
  int                             TotalTD, DynamicsStabilized;
  double                          perturb = -1.0, *y;

  Initialize(argc, argv);

  ReportNote("%-67s:  %d", "Maximum number of species allowed", MAX_SPECIES);
  ReportNote("%-67s:  %d", "# of basal species", BASALSPECIES);
  ReportNote("%-67s", "Food web topology and feeding ranges constructed following prey-predator body size ratios");
  ReportNote("%-67s", "Species body sizes selected randomly on a logarithmically scaled size interval");
  ReportNote("%-67s", "Food web built up at start by generating niche model network with maximum number of species");

#if (SSBM == 1)
#if   (STAGES == 3)
  ReportNote("%-67s", "Species dynamics follow stage-structured biomass model with 3 stage(s)");
#elif (STAGES == 2)
  ReportNote("%-67s", "Species dynamics follow stage-structured biomass model with 2 stage(s)");
#else
  ReportNote("%-67s", "Species dynamics follow unstructured biomass model");
#endif
#else
#if (STAGES == 2)
  ReportNote("%-67s", "Species dynamics follow stage-structured Lotka-Volterra type model");
#else
  ReportNote("%-67s", "Species dynamics follow unstructured Lotka-Volterra type model");
#endif
#endif

  ReportNote("%-67s", "All basal species have separate niches and forage on their own limiting resource");
  ReportNote("%-67s", "Cannibalism is EXPLICITLY prohibited to occur");
  ReportNote("%-67s", "Introduction density linearly decreasing with niche value");
  ReportNote("%-67s", "Type II functional response for all non-basal species");
  ReportNote("%-67s", "Non-basal species forage on all prey species in their food niche range unimodally");
  ReportNote("%-67s", "Maximum of the feeding kernel equals 1.0");
  ReportNote("%-67s", "Parameter variability of Alpha, Gamma, Maint and Mort governed by truncated normal distribution");
  ReportNote("%-67s", "All species have the same values for foraging and predation asymmetry");
#if ((SSBM == 0) && (STAGES == 1) && (ADJUSTJAFRAC == 1))
  ReportNote("%-67s", "Reproduction adjusted by constant juvenile-adult ratio (sqrt(5.0) - 1.0) / 2.0");
#endif
#if   (CONSTRATES == 1)
  ReportNote("%-67s", "Maturation rate is constant and equal to equilibrium rate");
#elif (CONSTRATES == 2)
  ReportNote("%-67s", "Fecundity is constant and equal to equilibrium rate");
#endif
#if   (QNA == 1)
  ReportNote("%-67s", "Quasi-neutral approximation of the structured dynamics following Rossberg & Farnsworth, 2010");
#endif

  ReportNote("");
  ReportNote("%-67s:  %ld", "Seeding value of the random-number generator", UniDevSeed);
  ReportNote("%-67s:  %f",  "Minimum species mortality rate", MIN_MORT);

  if (DynamicsRun)
    {
      if (argc == 4) perturb = atof(argv[3]);
      // Save the number of initial species for output
      InitialSpecies = Species;

      // Set the column of the species in the output and do some sanity checks on the interactions
      for (ii = 0, InitialBU = TotalTD = 0; ii < Species; ii++)
        {
          SpeciesCol[ii]  = ii;
          InitialBU      += PreyNr[ii];
          TotalTD        += PredNr[ii];

          if ((!PreyNr[ii]) && (ii >= BASALSPECIES))
            {
              char errmsg[REPORTNOTE_MAXLEN];
              sprintf(errmsg, "Unconnected species %d with ID %d left in the foodweb after re-sizing!", ii, SpeciesID[ii]);
              ProgramExit(1, errmsg);
            }
        }
      if (InitialBU != TotalTD) ProgramExit(1, "Total top-down and bottom-up links differ after re-sizing!");

      // Report on initial connectivity
      for (ii = 0, InitialLinks = 0; ii < Species; ii++)
        for (jj = 0; jj < Species; jj++)
          if (Phi[ii][jj] > MACH_PREC) InitialLinks++;

      ReportNote("%-67s:  %.1E", "Initial transient time interval without extinctions", 0.0);
      ReportNote("%-67s:  %f", "Initial connectivity", InitialLinks / ((double)(Species * Species)));
      if (perturb >= 0.0)
        {
          ReportNote("%-67s:  %f",  "All species perturbed with a factor", perturb);
          for (jj = 0; jj < Species; jj++) 
            {
              // Reset Total species density
              AduDens[jj] = dmax(AduDens[jj], MACH_PREC) * perturb;
#if (STAGES == 1)
              TotDens[jj] = AduDens[jj];
#elif (STAGES == 2)
              JuvDens[jj] = dmax(JuvDens[jj], MACH_PREC) * perturb;
              TotDens[jj] = JuvDens[jj] + AduDens[jj];
#else
              JuvDens[jj] = dmax(JuvDens[jj], MACH_PREC) * perturb;
              ImmDens[jj] = dmax(ImmDens[jj], MACH_PREC) * perturb;
              TotDens[jj] = JuvDens[jj] + ImmDens[jj] + AduDens[jj];
#endif
            }
        }
    }

  ReportNote("%-67s:  %.1E", "Transient time interval without extinctions", TRANSIENT_TIME);
  ReportNote("%-67s:  %.1E\n", "Time interval since last change before persistence is assumed", MIN_PERSIST_TIME);
  WriteReport(argc, argv);

  CheckExtinctions();

  // WebToCsbFile();

#if (JFRAC == 1)
  y = SpeciesDblData + (SPECIESDBLLEN - 3) * Species;
  for (ii = 0, jj = Species; ii < Species; ii++, jj++)
    {
      y[jj] = JuvDens[ii] + AduDens[ii];
      y[ii] = JuvDens[ii] / y[jj];
      y[ii + 2 * Species] = y[ii];                                                  // Save the initial juvenile fraction
    }

#if ((JFRACFIXED == 0) && ((CONSTRATES == 1) || (CONSTRATES == 2)))
  register int                    prey, pred;
  double                          resource;

  // Compute the initial maturation rate for all species
  for (pred = Species - 1; pred >= 0; pred--)
    {
      if ((JuvDens[pred] <= MACH_PREC) && (AduDens[pred] <= MACH_PREC)) continue;

      // First compute resource availability
      for (ii = 0, resource = 0; ii < PreyNr[pred]; ii++)
        {
          prey = prey_index[pred][ii];

          if ((JuvDens[prey] <= MACH_PREC) && (AduDens[prey] <= MACH_PREC)) continue;
          resource += (JuvAduSpec[pred]) * Phi[pred][prey] * JuvDens[prey];
          resource += (2.0 - JuvAduSpec[pred]) * Phi[pred][prey] * AduDens[prey];
        }

      if (pred < BASALSPECIES)
        {
          resource = PRODUCTIVITY / (TURNOVER + (QALPHA) * Alpha[pred] * JuvDens[pred] + (2.0 - QALPHA) * Alpha[pred] * AduDens[pred]);
          // If basal species do not compete divide total productivity by the number of
          // basal species to keep total system productivity the same
          resource /= BASALSPECIES;
        }
      else
        {
          resource  /= (Fhres[pred] + resource);
        }

#if   (CONSTRATES == 1)
      // Save the initial maturation rate in the location where for JFRACFIXED == 1 the initial juvenile fraction is stored
      y[pred + 2 * Species] = dmax(      (QALPHA) * Gamma[pred] * resource -       (QMAINT) * Maint[pred], 0);
#elif (CONSTRATES == 2)
      // Save the initial maturation rate in the location where for JFRACFIXED == 1 the initial juvenile fraction is stored
      y[pred + 2 * Species] = dmax((2.0 - QALPHA) * Gamma[pred] * resource - (2.0 - QMAINT) * Maint[pred], 0);
#endif
    }
#endif
#elif (QNA == 1)
  register int                    prey, pred;
  double                          resource, predation, maturation;
  double                          curpredmort[MAX_SPECIES];

  memset(curpredmort, 0, MAX_SPECIES * sizeof(double));
  for (pred = Species - 1; pred >= 0; pred--)
    {
      if ((JuvDens[pred] <= MACH_PREC) && (AduDens[pred] <= MACH_PREC)) continue;

      predation  = (QALPHA) * Alpha[pred] * JuvDens[pred];
      predation += (2.0 - QALPHA) * Alpha[pred] * AduDens[pred];

      // First compute resource availability
      for (ii = 0, resource = 0; ii < PreyNr[pred]; ii++)
        {
          prey = prey_index[pred][ii];

          if ((JuvDens[prey] <= MACH_PREC) && (AduDens[prey] <= MACH_PREC)) continue;

          resource += (JuvAduSpec[pred]) * Phi[pred][prey] * JuvDens[prey];
          resource += (2.0 - JuvAduSpec[pred]) * Phi[pred][prey] * AduDens[prey];
        }

      if (pred < BASALSPECIES)
        {
          resource = PRODUCTIVITY / (TURNOVER + predation);
          // If basal species do not compete divide total productivity by the number of
          // basal species to keep total system productivity the same
          resource /= BASALSPECIES;
        }
      else
        {
          predation /= (Fhres[pred] + resource);                                    // Ajust predation before resource
          resource  /= (Fhres[pred] + resource);
          for (ii = 0; ii < PreyNr[pred]; ii++)
            {
              prey = prey_index[pred][ii];
              if ((JuvDens[prey] <= MACH_PREC) && (AduDens[prey] <= MACH_PREC)) continue;

              curpredmort[prey] += Phi[pred][prey] * predation;
            }
        }

      // Save the initial maturation rate in the location where for JFRACFIXED == 1 the initial juvenile fraction is stored
      maturation  = dmax((QALPHA) * Gamma[pred] * resource - (QMAINT) * Maint[pred], 0);
      ReproVal[pred] = (Mort[pred] + (2.0 - JuvAduSpec[pred]) * curpredmort[pred]) / maturation;
      if (fabs(JuvDens[pred] / AduDens[pred] - ReproVal[pred]) > 1.0E-4)
        {
          (void)fprintf(stderr, "\nError: Species %d (ID: %d): Juvenile ratio (%.6f) unequal to expected equilibrium value (%6f)\n",
                        pred, SpeciesID[pred], JuvDens[pred] / AduDens[pred], ReproVal[pred]);
          (void)fprintf(stderr, "...now exiting to system...\n");
          exit(1);
        }
      Jwght[pred] = maturation / (maturation + 2 * (curpredmort[pred] + Mort[pred]));
      Awght[pred] = (maturation + JuvAduSpec[pred] * curpredmort[pred] + Mort[pred])/ (maturation + 2 * (curpredmort[pred] + Mort[pred]));
    }
#endif

  DynamicsStabilized = 0;
  if (DynamicsRun == 0) 
    {
#if (JFRAC == 1)
      y = SpeciesDblData + (SPECIESDBLLEN - 3) * Species;
#else
      y = SpeciesDblData + 2 * Species;
#endif
      odeint(y, STAGES*Species, &CurrentTime, TRANSIENT_TIME, Accuracy, SMALLEST_STEP, LARGEST_STEP, Gradient);
      UpdateVars(CurrentTime);

      DynamicsStabilized = CheckExtinctions();
    }
  else DynamicOutput();

  while (CurrentTime < MaxTime)
    {
#if (JFRAC == 1)
      y = SpeciesDblData + (SPECIESDBLLEN - 3) * Species;
#else
      y = SpeciesDblData + 2 * Species;
#endif
      odeint(y, STAGES*Species, &CurrentTime, CurrentTime + OutputInterval, Accuracy, SMALLEST_STEP, LARGEST_STEP, Gradient);
      UpdateVars(CurrentTime);

      DynamicsStabilized = CheckExtinctions();
      if (DynamicsRun) DynamicOutput();

      // fprintf(stderr, "Current integration time: %10.0f\t Number of species: %4d\t Seconds elapsed: %10.0f\n", CurrentTime, Species, difftime(time(0), t0) * 1000.);

      if (DynamicsStabilized || iszero(CurrentTime-MaxTime) || (CurrentTime > MaxTime)) break;
    }

  SummaryOutput();

  ProgramExit(0, "");

  return 0;
}

//==================================================================================
