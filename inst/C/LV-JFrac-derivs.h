/***
   NAME
     LV-Jfrac-derivs.h
   PURPOSE
     Derivatives routine for the 2-stage Lotka-Volterra type foodweb model in 
     terms of the total consumer density and the juvenile fraction.
     
     This implementation is based on the assumption that the background
     mortality is stage-independent

   Last modification: AMdR - Oct 29, 2020
***/

static double                     effpred[MAX_SPECIES];
static double                     effprey[MAX_SPECIES];

void Gradient(double time, double *y, double *dydt)

{
  register int                    ii, prey, pred;
  double                          resource;
  double                          adunet, reproduction;
  register double                 *totdens, *dtotdensdt;
  double                          juvnet, maturation;
  register double                 *juvfrac, *djuvfracdt;

#if (JFRACFIXED == 0)
  juvfrac    = y;
#else
  juvfrac    = SpeciesDblData + (SPECIESDBLLEN - 1) * Species;
#endif
  djuvfracdt = dydt;

  totdens    = y + (STAGES - 1) * Species;
  dtotdensdt = dydt + (STAGES - 1) * Species;

  for (pred = 0; pred < Species; pred++)
    {
#if (JFRACFIXED == 0)
      juvfrac[pred] = dmax(juvfrac[pred], 0.0);
      juvfrac[pred] = dmin(juvfrac[pred], 1.0);

#endif

      totdens[pred] = dmax(totdens[pred], 0.0);

      djuvfracdt[pred] = 0.0;
      dtotdensdt[pred] = -Mort[pred] * totdens[pred];                               // Background mortality

      if (totdens[pred] <= MACH_PREC) 
        {
          totdens[pred]  = 0.0;
          effpred[pred]  = 0.0;
        }
      else
        {
          effpred[pred]  = (QALPHA * juvfrac[pred] + (2.0 - QALPHA) * (1 - juvfrac[pred]));
          effpred[pred] *= Alpha[pred] * totdens[pred];
        }
    }

  // Set derivatives for all species
  // Start at the species with the highest index, which is the top-most consumer
  // When derivatives of a species are being computed, predation mortality is alreayd known
  for (pred = Species - 1; pred >= 0; pred--)
    {
      if (totdens[pred] <= MACH_PREC) continue;

      memset(effprey, 0, PreyNr[pred] * sizeof(double));
      // First compute resource availability
      for (ii = 0, resource = 0; ii < PreyNr[pred]; ii++)
        {
          prey = prey_index[pred][ii];
          if (totdens[prey] <= MACH_PREC) continue;

          effprey[ii]  = (JuvAduSpec[pred] * juvfrac[prey] + (2.0 - JuvAduSpec[pred]) * (1.0 - juvfrac[prey]));
          effprey[ii] *= Phi[pred][prey];
          resource    += effprey[ii] * totdens[prey];
        }

      if (pred < BASALSPECIES)
        {
          resource  = PRODUCTIVITY/(TURNOVER + effpred[pred]);
          // If basal species do not compete divide total productivity by the number of
          // basal species to keep total system productivity the same
          resource /= BASALSPECIES;
        }
      else
        {
          effpred[pred] /= (Fhres[pred] + resource);                                // Ajust predation before resource
          resource      /= (Fhres[pred] + resource);
          for (ii = 0; ii < PreyNr[pred]; ii++)
            {
              prey = prey_index[pred][ii];

              if (totdens[prey] <= MACH_PREC) continue;

              dtotdensdt[prey] -= effpred[pred] * effprey[ii] * totdens[prey];

#if (JFRACFIXED == 0)
                {
                  effprey[ii]       = 2.0 * (1.0 - JuvAduSpec[pred]) * (1.0 - juvfrac[prey]) * juvfrac[prey];
                  effprey[ii]      *= Phi[pred][prey];
                  djuvfracdt[prey] += effpred[pred] * effprey[ii];
                }
#endif
            }
        }

      juvnet       =       (QALPHA) * Gamma[pred] * resource -       (QMAINT) * Maint[pred];
      maturation   = dmax(juvnet, 0);

      adunet       = (2.0 - QALPHA) * Gamma[pred] * resource - (2.0 - QMAINT) * Maint[pred];
      reproduction = dmax(adunet, 0);

#if (JFRACFIXED == 0)
#if   (CONSTRATES == 1)
//      if (pred >= BASALSPECIES)
      maturation   = *(SpeciesDblData + (SPECIESDBLLEN - 1) * Species + pred);
#elif (CONSTRATES == 2)
//      if (pred >= BASALSPECIES)
      reproduction = *(SpeciesDblData + (SPECIESDBLLEN - 1) * Species + pred);
#endif
        {
          djuvfracdt[pred] += reproduction * (1 - juvfrac[pred]) * (1 - juvfrac[pred]);
          djuvfracdt[pred] -= maturation * juvfrac[pred];
        }
#endif

      dtotdensdt[pred] += reproduction * (1 - juvfrac[pred]) * totdens[pred];
    }

  return;
}

//==================================================================================
