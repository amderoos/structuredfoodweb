/***
   NAME
     LV-derivs.h
   PURPOSE
     Derivatives routine for the 1- or 2-stage Lotka-Volterra type foodweb model
     
   Last modification: AMdR - Nov 03, 2020
***/

void Gradient(double time, double *y, double *dydt)

{
  register int                    ii, prey, pred;
  double                          resource, predation;
  double                          adunet, reproduction;
  register double                 *adudens, *dadudensdt;
#if (STAGES > 1)
  double                          juvnet, juvmat;
  register double                 *juvdens, *djuvdensdt;

  juvdens    = y;
  djuvdensdt = dydt;

#endif
  adudens    = y + (STAGES - 1) * Species;
  dadudensdt = dydt + (STAGES - 1) * Species;

  for (pred = 0; pred < Species; pred++)
    {
      dadudensdt[pred] = -(2.0 - QMORT) * Mort[pred] * dmax(adudens[pred], 0);      // Background adult mortality

      if (adudens[pred] <= MACH_PREC) adudens[pred] = 0.0;
#if (QNA == 1)
      juvdens[pred] = ReproVal[pred] * adudens[pred];
#endif
#if (STAGES > 1)
      djuvdensdt[pred] = -(QMORT) * Mort[pred] * dmax(juvdens[pred], 0);            // Background juvenile mortality

      if (juvdens[pred] <= MACH_PREC) juvdens[pred] = 0.0;
#endif
    }

  // Set derivatives for all species
  // Start at the species with the highest index, which is the top-most consumer
  // When derivatives of a species are being computed, predation mortality is alreayd known
  for (pred = Species - 1; pred >= 0; pred--)
    {
      predation = 0.0;

#if (STAGES == 1)
      if (adudens[pred] <= MACH_PREC) continue;
#else
      if ((juvdens[pred] <= MACH_PREC) && (adudens[pred] <= MACH_PREC)) continue;

      predation        += (QALPHA) * Alpha[pred] * juvdens[pred];
#endif
      predation        += (2.0 - QALPHA) * Alpha[pred] * adudens[pred];

      // First compute resource availability
      for (ii = 0, resource = 0; ii < PreyNr[pred]; ii++)
        {
          prey = prey_index[pred][ii];

#if (STAGES == 1)
          if (adudens[prey] <= MACH_PREC) continue;
#else
          if ((juvdens[prey] <= MACH_PREC) && (adudens[prey] <= MACH_PREC)) continue;
          resource += (JuvAduSpec[pred]) * Phi[pred][prey] * juvdens[prey];
#endif
          resource += (2.0 - JuvAduSpec[pred]) * Phi[pred][prey] * adudens[prey];
        }

      if (pred < BASALSPECIES)
        {
          resource = PRODUCTIVITY / (TURNOVER + predation);
          // If basal species do not compete divide total productivity by the number of
          // basal species to keep total system productivity the same
          resource /= BASALSPECIES;
        }
      else
        {
          predation /= (Fhres[pred] + resource);                                    // Ajust predation before resource
          resource  /= (Fhres[pred] + resource);
          for (ii = 0; ii < PreyNr[pred]; ii++)
            {
              prey = prey_index[pred][ii];
#if (STAGES == 1)
              if (adudens[prey] <= MACH_PREC) continue;
#else
              if ((juvdens[prey] <= MACH_PREC) && (adudens[prey] <= MACH_PREC)) continue;
              djuvdensdt[prey] -= JuvAduSpec[pred] * Phi[pred][prey] * predation * juvdens[prey];
#endif
              dadudensdt[prey] -= (2.0 - JuvAduSpec[pred]) * Phi[pred][prey] * predation * adudens[prey];
            }
        }

      adunet       = (2.0 - QALPHA) * Gamma[pred] * resource - (2.0 - QMAINT) * Maint[pred];
      reproduction = dmax(adunet, 0) * adudens[pred];

#if (STAGES == 1)
      // Multiply with adult fraction (sqrt(5.0) - 1.0) / 2.0 ?
#if (ADJUSTJAFRAC == 1)
      reproduction     *= (sqrt(5.0) - 1.0) / 2.0;
#endif
      dadudensdt[pred] += reproduction;
#else
      juvnet = (QALPHA) * Gamma[pred] * resource - (QMAINT) * Maint[pred];

      juvmat = dmax(juvnet, 0) * juvdens[pred];

      djuvdensdt[pred] += reproduction - juvmat;
      dadudensdt[pred] += juvmat;

#if (QNA == 1)
      dadudensdt[pred] *= Awght[pred];
      dadudensdt[pred] += Jwght[pred] * djuvdensdt[pred];
      djuvdensdt[pred]  = 0;
#endif

#endif
    }

  return;
}

//==================================================================================
