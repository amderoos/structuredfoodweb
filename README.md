# StructuredFoodweb

This repository contains the data files, functions and R scripts that are used in the manuscript:

**Dynamic population stage-structure due to juvenile-adult asymmetry stabilizes complex ecological communities**

**Andre M. de Roos**

**Institute for Biodiversity and Ecosystem Dynamics, University of Amsterdam 1090 GE Amsterdam, The Netherlands, and Santa Fe Institute, Santa Fe, New Mexico 87501, USA**

### Installation

You can install this package by issuing the following command from the R command line:
```
install.packages("devtools")
devtools::install_git("https://bitbucket.org/amderoos/structuredfoodweb")
```

Notice, however, that this package has been created using MacOS and is not been tested on Windows machines

*****
