/***
   NAME
     web.c
   PURPOSE
     Routines involved in construction of the food web and the reconstruction of the
     interaction matrix
     
   Last modification: AMdR - Nov 04, 2020
***/

#include "foodweb.h"

/*
 *==================================================================================
 *
 *  ROUTINES FOR GENERATING RANDOM DEVIATES
 *
 *==================================================================================
 */

#if (!defined(GSLLIB) || (GSLLIB == 0))
#include "UniDev-NumRec.h"
#else
#include "UniDev-gsl.h"
#endif

//==================================================================================
/*
 * TruncNormDev - Routine returns a random number from a truncated normal
 *        distribution with given mean and width. Uses the function
 *        UniDev() to get three uniform deviates.
 *
 * Return -  the random number.
 */

static double TruncNormDev(double mean, double width)

{
  double chance1, chance2, chance3, disp;

  chance1 = UniDev();
  chance2 = UniDev();
  chance3 = UniDev();

  // Notice that disp varies between -width (if all probs are 0) and width
  // (if all probs are 1). Width is hence half the width of the kernel
  disp = (2.0 * width) * (chance1 + chance2 + chance3) / 3.0 - width;

  return (mean + disp);
}

//==================================================================================
/*
 * TruncNormDens - Routine returns the value of the probability density
 *        function of the Bates distribution on the interval [a,b]
 *
 *        ~/maple/foodweb/TruncatedNormal.mw
 *
 *        for further details.
 *
 * Return -  the value of the probability density function
 */

static double TruncNormDens(double y, double a, double b)

{
  double val, x;

  x = (y - a) / (b - a);

  if (x < 0.0)
    val = 0;
  else if (x < 1.0 / 3.0)
    val = 13.5 * x * x;
  else if (x < 2.0 / 3.0)
    val = -27.0 * x * x + 27.0 * x - 4.5;
  else if (x < 1)
    val = 13.5 * (x - 1) * (x - 1);
  else
    val = 0.0;

  val /= (b - a);

  return (val);
}

//==================================================================================

void ConstructInteractionMatrix()

{
  int                             prey, pred, prey_nr;

  // Compute the interaction matrices Phi[][] and prey_index[][]
  // Set all to entries to 0
  memset((void *)Phi,        0, (MAX_SPECIES * MAX_SPECIES) * sizeof(double));
  memset((void *)prey_index, 0, (MAX_SPECIES * MAX_SPECIES) * sizeof(int));

  memset((void *)PreyNr, 0, MAX_SPECIES * sizeof(int));
  memset((void *)PredNr, 0, MAX_SPECIES * sizeof(int));

  for (pred = BASALSPECIES; pred < Species; pred++)
    {
      prey_nr = 0;
      for (prey = 0; prey < Species; prey++)
        {
          if (prey == pred) continue;                                               // Exclude cannibalism
          if ((NicheVal[prey] >= MinFoodNiche[pred]) && (NicheVal[prey] <= MaxFoodNiche[pred]))
            {
              Phi[pred][prey] = TruncNormDens(NicheVal[prey], MinFoodNiche[pred], MaxFoodNiche[pred]);

              // The original niche model assumes feeding probability 1 across the entire range.
              // Multiply with the feeding range width to keep the surface under the curve the same
              // (= the feeding range width in the original model; surface under TruncNormDens() always equals 1).
              Phi[pred][prey] *= (MaxFoodNiche[pred] - MinFoodNiche[pred]);

              // The above makes the maximum of the curve equal to the maximum of the unit kernel
              // which is 2.25. Divide by this vaue to keep the maximum at 1.0
              Phi[pred][prey] /= 2.25;
              prey_index[pred][prey_nr] = prey;
              prey_nr++;
              PredNr[prey] += 1;
            }
        }
      PreyNr[pred] = prey_nr;
    }

  return;
}

//==================================================================================

int RearrangeMem(int oldSpecies, int newSpecies)

{
  int                             ii, jj, kk;

  // Skip the following lines at program start up (oldSpecies == 0 && newSpecies == MAX_SPECIES)
  if (!((oldSpecies == 0) && (newSpecies == MAX_SPECIES)))
    {
      if (oldSpecies < newSpecies) ProgramExit(1, "New species number larger than previous species number");

      // Compact all data so that it is contiguous. Skip the first row, as it is already in place
      for (ii = 1, jj = oldSpecies, kk = newSpecies; ii < SPECIESDBLLEN; ii++, jj += oldSpecies, kk += newSpecies) 
        memmove(SpeciesDblData + kk, SpeciesDblData + jj, newSpecies*sizeof(double));
      for (ii = 1, jj = oldSpecies, kk = newSpecies; ii < SPECIESINTLEN; ii++, jj += oldSpecies, kk += newSpecies) 
        memmove(SpeciesIntData + kk, SpeciesIntData + jj, newSpecies*sizeof(int));
    }

  // Now reset the pointers to the appropriate locations
  ii = 0;
  SpeciesID       = SpeciesIntData + ii; ii += newSpecies;
  SpeciesCol      = SpeciesIntData + ii; ii += newSpecies;
  PreyNr          = SpeciesIntData + ii; ii += newSpecies;
  PredNr          = SpeciesIntData + ii; ii += newSpecies;

  ii = 0;
  TotDens         = SpeciesDblData + ii; ii += newSpecies;
  SpeciesAge      = SpeciesDblData + ii; ii += newSpecies;

#if (STAGES > 1)
  JuvDens         = SpeciesDblData + ii; ii += newSpecies;
#if (STAGES> 2)
  ImmDens         = SpeciesDblData + ii; ii += newSpecies;
#endif
#endif
  AduDens         = SpeciesDblData + ii; ii += newSpecies;

  NicheVal        = SpeciesDblData + ii; ii += newSpecies;
  SpeciesBodySize = SpeciesDblData + ii; ii += newSpecies;

  Alpha           = SpeciesDblData + ii; ii += newSpecies;
  Fhres           = SpeciesDblData + ii; ii += newSpecies;
  JuvAduSpec      = SpeciesDblData + ii; ii += newSpecies;
  Gamma           = SpeciesDblData + ii; ii += newSpecies;
  Maint           = SpeciesDblData + ii; ii += newSpecies;
  Mort            = SpeciesDblData + ii; ii += newSpecies;

  MinFoodNiche    = SpeciesDblData + ii; ii += newSpecies;
  MaxFoodNiche    = SpeciesDblData + ii; ii += newSpecies;

#if (STAGES > 1)
  JuvDensMean     = SpeciesDblData + ii; ii += newSpecies;
  JuvDensVariance = SpeciesDblData + ii; ii += newSpecies;
  JuvDensMin      = SpeciesDblData + ii; ii += newSpecies;
  JuvDensMax      = SpeciesDblData + ii; ii += newSpecies;
#if (STAGES> 2)
  ImmDensMean     = SpeciesDblData + ii; ii += newSpecies;
  ImmDensVariance = SpeciesDblData + ii; ii += newSpecies;
  ImmDensMin      = SpeciesDblData + ii; ii += newSpecies;
  ImmDensMax      = SpeciesDblData + ii; ii += newSpecies;
#endif
#endif

  AduDensMean     = SpeciesDblData + ii; ii += newSpecies;
  AduDensVariance = SpeciesDblData + ii; ii += newSpecies;
  AduDensMin      = SpeciesDblData + ii; ii += newSpecies;
  AduDensMax      = SpeciesDblData + ii; ii += newSpecies;

  TotDensMean     = SpeciesDblData + ii; ii += newSpecies;
  TotDensVariance = SpeciesDblData + ii; ii += newSpecies;
  TotDensMin      = SpeciesDblData + ii; ii += newSpecies;
  TotDensMax      = SpeciesDblData + ii; ii += newSpecies;

  return newSpecies;
}

//==================================================================================

static void SetSpeciesPars(int newsp, double nichevalue)

{
  double                          logsize, quart = 1.0;
  double                          feedmid, feedrange;
  double                          LogMinSize, LogMaxSize;

  // Total size range width on a log scale
  LogMinSize  = log10(MIN_SIZE);
  LogMaxSize  = log10(MAX_SIZE);

  // Sets the species-specific parameters for intake, maintenance, mortality,
  // converison efficiency and characteristic prey-predator size ratio
  SpeciesID[newsp] = newsp + 1;

  // The niche model assumes niche values Ni scaled between 0 and 1
  NicheVal[newsp] = nichevalue;

  // Assume the niche value maps onto a logarithmic size range between minimum and maximum
  logsize = LogMinSize + (LogMaxSize - LogMinSize) * nichevalue;
  SpeciesBodySize[newsp] = pow(10.0, logsize);

  quart = pow(SpeciesBodySize[newsp], -0.25);
  Alpha[newsp]      = ALPHA_CONST * TruncNormDev(1.0, ALPHA_WIDTH) * quart;
  Fhres[newsp]      = FHRES_CONST + (2 * UniDev() - 1) * FHRES_WIDTH;
  Maint[newsp]      = MAINT_CONST * TruncNormDev(1.0, MAINT_WIDTH) * quart;
  Gamma[newsp]      = GAMMA_CONST * TruncNormDev(1.0, GAMMA_WIDTH) * quart;
  Mort[newsp]       = MORT_CONST * TruncNormDev(1.0, MORT_WIDTH) * quart;
  Mort[newsp]       = dmax(Mort[newsp], MIN_MORT);
  JuvAduSpec[newsp] = JUVADUSPEC_CONST + (2 * UniDev() - 1) * JUVADUSPEC_WIDTH;

  if (newsp >= BASALSPECIES)
    {
      // The midpoint of the feeding range is drawn uniformly from
      // [Ni + MED_LOGPPMR_MIN/(LogMaxSize-LogMinSize), Ni + MED_LOGPPMR_MAX/(LogMaxSize-LogMinSize)]
      //
      // Notice default value for MED_LOGPPMR_MIN and MED_LOGPPMR_MAX are -2.5 and -0.5, respectively.
      feedmid   = NicheVal[newsp] + (MED_LOGPPMR_MIN + UniDev() * (MED_LOGPPMR_MAX - MED_LOGPPMR_MIN)) / (LogMaxSize - LogMinSize);
      feedrange = 2.0 * LOGPPMR_WIDTH / (LogMaxSize - LogMinSize);

      MinFoodNiche[newsp] = feedmid - 0.5 * feedrange;
      MaxFoodNiche[newsp] = feedmid + 0.5 * feedrange;
    }
  else
    {
      MinFoodNiche[newsp] = 0;
      MaxFoodNiche[newsp] = 0;
    }

  // Introduce species at low density, with the density linearly decreasing
  // with their niche value
  TotDens[newsp] = INTRO_DENSITY * (1.0 - NicheVal[newsp]);
#if (STAGES == 1)
  AduDens[newsp] = TotDens[newsp] / STAGES;
#elif (STAGES == 2)
  JuvDens[newsp] = TotDens[newsp] / STAGES;
  AduDens[newsp] = TotDens[newsp] / STAGES;
#else
  JuvDens[newsp] = TotDens[newsp] / STAGES;
  ImmDens[newsp] = TotDens[newsp] / STAGES;
  AduDens[newsp] = TotDens[newsp] / STAGES;
#endif

  return;
}

//==================================================================================

void BuildWeb()

{
  int                             ii, jj, kk, oldSpecies, newSpecies;
  int                             TotalTD, prey_nr, OldNoLinks, NoLinks;
  double                          newnicheval, nichevalues[MAX_SPECIES];

  // Initialize some variables
  for (ii = 0; ii < MAX_SPECIES; ii++) nichevalues[ii] = DBL_MAX;

  Species = MAX_SPECIES;

  // Sets the niche values of all new species
  for (ii = 0; ii < Species; ii++)
    {
      // Select niche value randomly from [0, 1]
      newnicheval = UniDev();

      // Sort the new niche value in the row
      jj = 0;
      while ((jj < ii) && (newnicheval > nichevalues[jj])) jj++;
      for (kk = ii; kk > jj; kk--) nichevalues[kk] = nichevalues[kk - 1];
      nichevalues[jj] = newnicheval;
    }

  // Now initialize parameters for all consumer species
  for (ii = 0; ii < Species; ii++)
    SetSpeciesPars(ii, nichevalues[ii]);

  // Construct the basic interaction matrix
  NoLinks = 0;
  oldSpecies = Species;
  while (1)
    {
      ConstructInteractionMatrix();

      OldNoLinks = NoLinks;
      // Weed out non-basal species with unconnected stages
      for (newSpecies = BASALSPECIES, jj = BASALSPECIES; jj < Species; jj++)
        {
          prey_nr = PreyNr[jj];
          if (!prey_nr)
            {
              NoLinks++;
              TotDens[jj] = -1.0;
              continue;                                                             // Source species is extinct, do not copy
            }
          if (newSpecies != jj)                                                     // In-between species have been removed: copy
            {
              for (ii = 0; ii < SPECIESDBLLEN; ii++) 
                SpeciesDblData[ii*MAX_SPECIES + newSpecies] = SpeciesDblData[ii*MAX_SPECIES + jj];
              for (ii = 0; ii < SPECIESINTLEN; ii++) 
                SpeciesIntData[ii*MAX_SPECIES + newSpecies] = SpeciesIntData[ii*MAX_SPECIES + jj];
            }
          newSpecies++;
        }
      Species = newSpecies;

      // This while loop continuous until there are no more links lost.
      if (OldNoLinks == NoLinks) break;
    }

  if (Species < oldSpecies) Species = RearrangeMem(oldSpecies, Species);

  // Save the number of initial species for output
  InitialSpecies = Species;

  // Do some sanity checks on the interactions
  for (ii = 0, InitialBU = TotalTD = 0; ii < Species; ii++)
    {
      prey_nr    = PreyNr[ii];
      InitialBU += prey_nr;
      TotalTD   += PredNr[ii];

      if ((!prey_nr) && (ii >= BASALSPECIES))
        ProgramExit(1, "Unconnected species left in the foodweb after re-sizing!");
    }
  if (InitialBU != TotalTD)
    ProgramExit(1, "Total top-down and bottom-up links differ after re-sizing!");

  // Report on initial connectivity
  for (ii = 0, InitialLinks = 0; ii < Species; ii++)
    for (jj = 0; jj < Species; jj++)
      if (Phi[ii][jj] > MACH_PREC)
        InitialLinks++;

  ReportNote("%-67s:  %f", "Initial connectivity", InitialLinks / ((double)(Species * Species)));
  ReportNote("%-67s:  %d", "Number of unlinked species removed", NoLinks);

  return;
}

//==================================================================================
