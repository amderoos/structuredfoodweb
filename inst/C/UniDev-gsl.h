/***
   NAME
     UniDev-gsl.h
   PURPOSE
     Routine returns a uniform random deviate between 0.0 and 1.0 using the 
     system indpendent routine double precision output (48 bits) from the 
     RANLXS generator in the gsl library.
     
   Last modification: AMdR - Feb 26, 2020
***/

#include "gsl/gsl_rng.h"
#include "sys/time.h"
#include "time.h"

double UniDev(void)

{
  static int iff = 0;
  static gsl_rng *rng;

  if (iff == 0)
    {
      struct timeval tptr;
      struct timezone tzptr;
      const gsl_rng_type *T;

      // Initialization of the random number generator
      if (!UniDevSeed)
        {
          gettimeofday(&tptr, &tzptr);
          UniDevSeed = tptr.tv_usec;
        }

      // Double precision output (48 bits) from the RANLXS generator
      T = gsl_rng_ranlxd2;
      rng = gsl_rng_alloc(T);

      gsl_rng_set(rng, UniDevSeed);
    }
  iff = 1;

  // Generates random values in [0,1)
  return gsl_rng_uniform(rng);
}

//==================================================================================
