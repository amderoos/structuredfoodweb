/***
   NAME
     odeint-rkqw
   DESCRIPTION
     Cash-Karp Runge-Kutta integration step of the system and updates all the
     variables. 

     Implementation follows the routine rkqs from Numerical Recipes but uses a 
     different error checking algorithm, which follows Watts & Shampine 
     (see Forsythe et al., 1977, Computer methods for mathematical computations, 
     Chapter 6).

   Last modification: AMdR - Feb 21, 2020
***/
#ifndef ODE_RKQW
#define ODE_RKQW

#include "foodweb.h"

/*
 *==================================================================================
 * Macro definitions that can be (p)re-defined by the user
 *==================================================================================
 */

#ifndef MAXSTP
#define MAXSTP                    1000000
#endif
#ifndef INIT_H
#define INIT_H                    0.01
#endif

/*
 *==================================================================================
 * Macro definitions needed in integration routine
 *==================================================================================
 */

#define BAD                       0
#define OK                        1
#define TINY                      DBL_EPSILON

/*
 *==================================================================================
 * Global variables needed in integration routine (system independent)
 *==================================================================================
 */

static double                     *yerr;
static double                     *ynew;
static double                     *ak2;
static double                     *ak3;
static double                     *ak4;
static double                     *ak5;
static double                     *ak6;
static double                     *ytemp;

static long int                   Ode_Dim, naccpt, nrejct, nfcn;

static const double a2 = 0.2, a3 = 0.3, a4 = 0.6, a5 = 1.0, a6 = 0.875,
                    b21 = 0.2, 
                    b31 = 3.0 / 40.0,         b32 = 9.0 / 40.0, 
                    b41 = 0.3,                b42 = -0.9,               b43 = 1.2, 
                    b51 = -11 / 54.0,         b52 = 2.5,                b53 = -70.0 / 27.0,
                    b54 = 35.0 / 27.0,
                    b61 = 1631.0 / 55296.0,   b62 = 175.0 / 512.0,      b63 = 575.0 / 13824.0,
                    b64 = 44275.0 / 110592.0, b65 = 253.0 / 4096.0,
                    c1  = 37.0 / 378.0,       c3 = 250.0 / 621.0,       c4 = 125.0 / 594.0,
                    c6  = 512.0 / 1771.0, 
                    dc5 = -277.0 / 14336.0;
static const double dc1 = 37.0 / 378.0 - 2825.0 / 27648.0,
                    dc3 = 250.0 / 621.0 - 18575.0 / 48384.0,
                    dc4 = 125.0 / 594.0 - 13525.0 / 55296.0,
                    dc6 = 512.0 / 1771.0 - 0.25;

//==================================================================================

static void rkck(double x, double *y, double *dydx, double h,
                 double *yout, double *yerr,
                 void (*derivs)(double, double *, double *))
{
  register int                    ii;

  for (ii = 0; ii < Ode_Dim; ii++)
    ytemp[ii] = y[ii] + b21 * h * dydx[ii];                                         // first step

  (*derivs)(x + a2 * h, ytemp, ak2);                                                // second step
  nfcn += 1;
  for (ii = 0; ii < Ode_Dim; ii++)
    ytemp[ii] = y[ii] + h * (b31 * dydx[ii] + b32 * ak2[ii]);

  (*derivs)(x + a3 * h, ytemp, ak3);                                                // third step
  nfcn += 1;
  for (ii = 0; ii < Ode_Dim; ii++)
    ytemp[ii] = y[ii] + h * (b41 * dydx[ii] + b42 * ak2[ii] + b43 * ak3[ii]);

  (*derivs)(x + a4 * h, ytemp, ak4);                                                // fourth step
  nfcn += 1;
  for (ii = 0; ii < Ode_Dim; ii++)
    ytemp[ii] = y[ii] + h * (b51 * dydx[ii] + b52 * ak2[ii] + b53 * ak3[ii] + b54 * ak4[ii]);

  (*derivs)(x + a5 * h, ytemp, ak5);                                                // fifth step
  nfcn += 1;
  for (ii = 0; ii < Ode_Dim; ii++)
    ytemp[ii] = y[ii] + h * (b61 * dydx[ii] + b62 * ak2[ii] + b63 * ak3[ii] + b64 * ak4[ii] + b65 * ak5[ii]);

  (*derivs)(x + a6 * h, ytemp, ak6);                                                // sixth step
  nfcn += 1;
  for (ii = 0; ii < Ode_Dim; ii++)
    yout[ii] = y[ii] + h * (c1 * dydx[ii] + c3 * ak3[ii] + c4 * ak4[ii] + c6 * ak6[ii]);

  for (ii = 0; ii < Ode_Dim; ii++)
    yerr[ii] = h * (dc1 * dydx[ii] + dc3 * ak3[ii] + dc4 * ak4[ii] + dc5 * ak5[ii] + dc6 * ak6[ii]);

  return;
}

//==================================================================================

#define SAFETY                    0.90
#define PGROW                     -0.20
#define PSHRINK                   -0.25
#define ERRCON                    1.889568E-4

static int rkqw(double *x, double *y,
                double htry, double eps,
                double *dydx,  double *hdid, double *hnext,
                void (*derivs)(double, double *, double *))
{
  register int                    ii;
  double                          err, errmax, h;
  double                          abs_err;
  static int                      step_failed = 0;

  h = htry;
  abs_err = (2.0E-13) / eps;
  for (;;)
    {
      rkck(*x, y, dydx, h, ynew, yerr, derivs);                                     // take a step
      errmax = 0.0;

      // Determine relative error  according to Watts & Shampine (see Forsythe)
      for (ii = 0; ii < Ode_Dim; ii++)
        {
          err = fabs(yerr[ii]) / (fabs(y[ii]) + fabs(ynew[ii]) + abs_err);
          if (err > errmax)  errmax = err;
        }
      errmax *= 2.0 * h / eps;

      if (errmax > 1.0)                                                             // if truncation error is too	large, reduce the stepsize
        {
          step_failed = 1;

          if (errmax < 59049.0)                                                     // from Watts & Shampine
            h *= SAFETY * pow(errmax, PSHRINK);
          else h *= 0.1;                                                            // Stepdecrease is limited to 0.1

          nrejct += 1;

          continue;
        }
      else                                                                          // step succeeded; compute size of next step
        {
          if (errmax > ERRCON)
            *hnext = SAFETY * pow(errmax, PGROW) * h;
          else *hnext = 5.0 * h;                                                    // Increase is limited to factor 5.0

          if (step_failed) *hnext = h;                                              // No increase if failed just before
          step_failed = 0;

          *hdid  = h;
          *x    += h;

          memcpy(y, ynew, Ode_Dim * sizeof(double));
          naccpt += 1;

          break;
        }
    }

  return (OK);
}

#undef PGROW
#undef PSHRNK
#undef SAFETY
#undef ERRCON

//==================================================================================

/***
   NAME
     odeint
   DESCRIPTION
     General Integration driver with adaptive stepsize control for the rkqc,
     rkqs, rkqw and pirkradau integration routine. Integrate starting
     values ystart[0..nvar-1] from 0 to xmax with accuracy eps. hmin should
     be set as the minimum allowed stepsize (can be zero), and hmax as the
     maximum allowed stepsize. 
     On output ystart is replaced by values at the end of the integration
     interval.
***/

void odeint(double *ystart, int vecdim, double *xinit, double xmax,
            double eps, double hmin, double hmax,
            void (*derivs)(double, double *, double *))

{
  long int                        nstp;
  int                             succes;
  double                          x, hnext, hdid;
  static double                   h = 0;
  static int                      first = 1;   
  double                          *y, *dydx;

  Ode_Dim = vecdim;
  memset(ODEMemPnt, 0, ODE_VECTOR_COPIES * Ode_Dim * sizeof(double));

  y     = ODEMemPnt;
  dydx  = y     + Ode_Dim;
  yerr  = dydx  + Ode_Dim;
  ynew  = yerr  + Ode_Dim;
  ak2   = ynew  + Ode_Dim;
  ak3   = ak2   + Ode_Dim;
  ak4   = ak3   + Ode_Dim;
  ak5   = ak4   + Ode_Dim;
  ak6   = ak5   + Ode_Dim;
  ytemp = ak6   + Ode_Dim;

  if (first)
    {
      h = (xmax > 0.0) ? fabs(INIT_H) : -fabs(INIT_H);
    }
  first = 0;

  x = *xinit;
  memcpy(y, ystart, Ode_Dim*sizeof(double));

  naccpt = nrejct = nfcn = 0L;

  for (nstp = 0; nstp < MAXSTP; nstp++)                                             // Take at most MAXSTP steps
    {
      (*derivs)(x, y, dydx);
      nfcn += 1;

      if ((x + h - xmax) * (x + h) > 0.0) h = xmax - x;

      succes = rkqw(&x, y, h, eps, dydx, &hdid, &hnext, derivs);

      if (succes != OK)
        {
          (void)fprintf(stderr, "Step size too small in routine RKQC\n\n");
          (void)fprintf(stderr, "...now exiting to system...\n\n");
          exit(1);
        }

      h = (hnext < hmax) ? hnext : hmax;

      if ((xmax - x) < SMALLEST_STEP)
        {
          memcpy(ystart, y, Ode_Dim*sizeof(double));
          *xinit = x;
          return;                                                                   // Succesfull completion
        }

      if (fabs(hnext) <= hmin)
        {
          (void)fprintf(stderr, "Step size too small in ODEINT\n\n");
          (void)fprintf(stderr, "...now exiting to system...\n\n");
          exit(1);
        }
    }
  (void)fprintf(stderr, "Too many steps in routine ODEINT\n\n");
  (void)fprintf(stderr, "...now exiting to system...\n\n");
  exit(1);

  return;
}

//==================================================================================
#endif
