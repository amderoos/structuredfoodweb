********************************************************************************

  PROGRAM, RUN AND ARGUMENTS
    Program   : ./LVfoodweb_1j1a
    Run       : Q070-JAS18-P070-01
    Arguments :  545364

  MODEL SPECIFIC NOTES
    Initial connectivity                                               :  0.075250
    Number of unlinked species removed                                 :  35
    Maximum number of species allowed                                  :  500
    # of basal species                                                 :  1
    Food web topology and feeding ranges constructed following prey-predator body size ratios
    Species body sizes selected randomly on a logarithmically scaled size interval
    Food web built up at start by generating niche model network with maximum number of species
    Species dynamics follow stage-structured Lotka-Volterra type model 
    All basal species have separate niches and forage on their own limiting resource
    Cannibalism is EXPLICITLY prohibited to occur                      
    Introduction density linearly decreasing with niche value          
    Type II functional response for all non-basal species              
    Non-basal species forage on all prey species in their food niche range unimodally
    Maximum of the feeding kernel equals 1.0                           
    Parameter variability of Alpha, Gamma, Maint and Mort governed by truncated normal distribution
    All species have the same values for foraging and predation asymmetry
    
    Seeding value of the random-number generator                       :  545364
    Minimum species mortality rate                                     :  0.001000
    Transient time interval without extinctions                        :  1.0E+04
    Time interval since last change before persistence is assumed      :  1.0E+06


  USED VALUES FOR CONTROL VARIABLES
    Fixed step size or integration accuracy when adaptive              :  1E-07     
    Tolerance value, determining identity with zero                    :  1E-06     
    Maximum integration time                                           :  1000000000.00
    Output time interval:                        Only at events        :  5.00      

  USED VALUES OF PARAMETERS
    [ 0] Productivity of all basal species           PRODUCTIVITY = >= 0.3        :  70        
    [ 1] Turnover rate of all basal species              TURNOVER =    1.0        :  2         
    [ 2] Allometric scaling constant of attack rate   ALPHA_CONST =    1.0        :  1         
    [ 3] Relative width of its distribution           ALPHA_WIDTH =    0.1        :  0.1       
    [ 4] Functional response half-saturation density  FHRES_CONST =    1.5        :  1.5       
    [ 5] Absolute width of its distribution           FHRES_WIDTH =    1.0        :  1         
    [ 6] Juvenile/adult biomass in predation     JUVADUSPEC_CONST =    1.0        :  1.8       
    [ 7] Relative width of its distribution      JUVADUSPEC_WIDTH =    0.0        :  0         
    [ 8] Allometric scaling constant of production    GAMMA_CONST =    0.6        :  0.6       
    [ 9] Relative width of its distribution           GAMMA_WIDTH =    0.1        :  0.1       
    [10] Allometric scaling constant of maintenance   MAINT_CONST =    0.1        :  0.1       
    [11] Relative width of its distribution           MAINT_WIDTH =    0.1        :  0.1       
    [12] Allometric scaling constant of mortality      MORT_CONST =    0.015      :  0.015     
    [13] Relative width of its distribution            MORT_WIDTH =    0.1        :  0.1       
    [14] Adult-juvenile difference in Alpha                QALPHA =    1.0        :  0.7       
    [15] Adult-juvenile difference in Maint                QMAINT =    1.0        :  1         
    [16] Adult-juvenile difference in Gamma                QGAMMA =    1.0        :  0.7       
    [17] Adult-juvenile difference in Mort                  QMORT =    1.0        :  1         
    [18] Lowest median log10(PPMR)                MED_LOGPPMR_MIN =   -3.0        :  -2.5      
    [19] Highest median log10(PPMR)               MED_LOGPPMR_MAX =   -1.0        :  -0.5      
    [20] One-sided width of PPMR distribution       LOGPPMR_WIDTH =    1.0        :  0.5       
    [21] Juvenile-adult body size ratio           JUVADUSIZERATIO =    0.1        :  0.1       
    [-6] Seed value of the random number generator     UNIDEVSEED =   -1.0        :  5.454E+05 
    [-5] Minimum allowable body size                     MIN_SIZE =    1.0E-8     :  1E-08     
    [-4] Maximum allowable body size                     MAX_SIZE =    1.0        :  1E+04     
    [-3] Biomass introduction density                 INTRO_LEVEL =    1.0E-1     :  0.1       
    [-2] Extinction density                               EXTINCT =    1.0E-8     :  1E-08     
********************************************************************************
