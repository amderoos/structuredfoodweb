#'
#' Reads the contents of multiplee average files with extention .avg produced by the foodweb simulation model
#' and plots the resulting community sizes as a function of system productivity or another parameter
#'
#'   CommunityDynamicsPlot(filepat = NULL, webs = NULL, model = c("1s", "2s", "jf", "jfa", "jfb", "jfc", "qna", "ssbm3s"),
#'                         basal = 1, paneldim = 4.0, vertical = F, xlim = c(0, 100), ylim = c(0, 10),
#'                         perturb = NULL, log = FALSE, ...)
#'
#' @param  filepat  (single single or vector of two strings, required)
#' \preformatted{}
#'               Name(s) of the WEB.CSB file(s) with or without extension.
#'
#' @param  webs  (single numeric or vector of numeric values, optional)
#' \preformatted{}
#'               If specified, each elment should specify a seed number
#'               indicating the particular food wbe to plot from the CSB files.
#'               The length of this argument should match the length
#'               of the vector 'filepat'.
#'
#' @param  model  (string or vector of strings, optional)
#' \preformatted{}
#'               If specified should have the same length as the argument 'filepat'
#'               Specifies which model to run for computing the dynamics:
#'                   1s     : single-stage model without structure
#'                   2s     : Model with juvenile-adult stage structure in terms of numerical densities
#'                   jf     : Model with juvenile-adult stage structure in terms of total numerical density and juvenile fraction
#'                   jfa    : Model with juvenile-adult stage structure in terms of total numerical density and juvenile fraction with constant maturation
#'                   jfb    : Model with juvenile-adult stage structure in terms of total numerical density and juvenile fraction with constant reproduction
#'                   jfc    : Model with juvenile-adult stage structure in terms of total numerical density and constant juvenile fraction
#'                   qna    : Quasi-neutral approximation of the juvenile-adult stage structured model (Rossberg & Farnsworth, 2010)
#'                   ssbm3s : Stage-structured biomass model in terms of larval, juvenile and adult biomass
#'
#' @param  basal  (numeric value, optional)
#' \preformatted{}
#'               Number of basal species with which the results were computed
#'
#' @param  paneldim  (numeric, optional)
#' \preformatted{}
#'               Dimension of a single panel in inches
#'
#' @param  vertical  (boolean, optional)
#' \preformatted{}
#'               Whether panels should be arranged vertically
#'
#' @param  xlim  (numeric value, optional)
#' \preformatted{}
#'               Minimum and maximum value of the parameter on the x-axis
#'
#' @param  ylim  (numeric value, optional)
#' \preformatted{}
#'               Minimum and maximum value of the community sizes on the y-axis
#'
#' @param  perturb  (numeric value, optional)
#' \preformatted{}
#'               Factor with which to perturb a randomly selected species
#'
#' @param  log  (boolean, optional)
#' \preformatted{}
#'               Whether or not to plot the y-axis as a logarithmic axis
#'
#' @param  ...  (optional)
#' \preformatted{}
#'               Further arguments passed on to the plot commands
#'
#' @return List of list. Each sublist contains the results of a single computation of community dynamics
#'
#' @import graphics
#' @import RColorBrewer
#' @export
CommunityDynamicsPlot <- function(filepat = NULL, webs = NULL, model = "1s", basal = 1, paneldim = 4.0,
                                  vertical = FALSE, xlim = c(0, 1000), ylim = c(0, 10), perturb = NULL, log = FALSE, ...) {

  if (is.null(filepat) || (!length(filepat)) || (!nchar(filepat))) stop("You have to specify at least a single name of a .web.csb file")
  if ((length(filepat) != 1) && (length(filepat) != 2)) stop("You have to specify either 1 or 2 names of .web.csb files")
  if ((!is.null(webs)) && (length(webs) != length(filepat))) stop("The argument 'webs' should have length ", length(filepat))
  if (length(model) != length(filepat)) stop("The argument 'model' should have length ", length(filepat))
  if (!all(model %in% c("1s", "2s", "jf", "jfa", "jfb", "jfc", "qna", "ssbm3s"))) stop("The argument 'model' should only contain strings '1s', '2s', 'jf', 'jfa', 'jfb', 'jfc', 'qna' or 'ssbm3s'")
  if ((!is.null(perturb)) && (length(perturb) != length(filepat))) stop("The argument 'perturb' should have length ", length(filepat))
  dots <- list(...)
  if (!is.null(dots)) {
    unknown <- names(dots[!names(dots) %in% names(formals(graphics::plot.default))])
    if (length(unknown)>0) warning(paste("Unknown argument(s):", unknown, sep=" "))
  }
  oldwd <- getwd()
  tmpdir <- tempdir()

  dirpat <- dirname(filepat)
  filename <- paste0(gsub(".web.csb$", "", basename(filepat)), ".web.csb")
  filelist <- unlist(lapply(seq(length(filename)),
                            function(i) {
                              fns <- list.files(path = dirpat[i], pattern = filename[i], full.names = TRUE)
                              if (length(fns) == 0) {
                                dp <- paste0(system.file("extdata", package = "StructuredFoodweb"), "/", dirpat[i])
                                fns <- list.files(path = dp, pattern = filename[i], full.names = TRUE)
                              }
                              if (length(fns) == 0) stop(paste0("No files found matching the pattern ", filename[i]))
                              if (length(fns) > 1) stop(paste0("Pattern ", filename[i], " matches multiple filenames with extension .web.csb"))
                              return(fns)
                            }))
  nfiles <- length(filelist)

  def.par <- par(no.readonly = TRUE) # save default, for resetting...

  oneinch <- 2.54
  leftbotmai <- 1.0 # This is in inches!!!!
  rightmai <- 2.0 # This is in inches!!!!
  topmai <- 1.0 # This is in inches!!!!

  panwd <- c(lcm((paneldim+leftbotmai+rightmai)*oneinch))
  if ((!vertical) && (nfiles > 1)) {
    panwd <- c(lcm((paneldim+leftbotmai)*oneinch), lcm((paneldim+rightmai)*oneinch))
  }
  panhg <- c(lcm((paneldim+leftbotmai+topmai)*oneinch))
  if ((vertical) && (nfiles > 1)) {
    panhg <- c(lcm((paneldim+topmai)*oneinch), lcm((paneldim+leftbotmai)*oneinch))
  }

  if (nfiles > 1) {
    if (vertical) {
      layout(matrix((1:nfiles), nfiles, 1, byrow = TRUE), widths  = panwd, heights = panhg, respect=TRUE)
    } else {
      layout(matrix((1:nfiles), 1, nfiles, byrow = TRUE), widths  = panwd, heights = panhg, respect=TRUE)
    }
  } else {
    leftbotmai <- 1.0 # This is in inches!!!!
    rightmai <- 2.0 # This is in inches!!!!
    topmai <- 1.0 # This is in inches!!!!
  }

  g <- list()
  setwd(tmpdir)
  for (j in (1:nfiles)) {
    if (vertical) {
      par(mai=c((j == nfiles)*leftbotmai, leftbotmai, (j == 1)*topmai, rightmai))
    } else {
      par(mai=c(leftbotmai, (j == 1)*leftbotmai, topmai, (j == nfiles)*rightmai))
    }

    ### Compute the dynamics
    csbname <- paste0(gsub(".web.csb$", "", filelist[j]), ".web.csb")
    if (!file.exists(csbname)) stop(paste0("File: ", csbname, " not found"))

    if (!is.null(webs)) {
      seednr <- webs[j]
      state <- csbread(csbname, paste0("State-", sprintf("%.6E", seednr)))
    } else {
      sink("/dev/null"); statenr <- csbread(csbname); sink();
      if (is.null(statenr)) stop(paste0("File: ", csbname, " can not be read by csbread()"))
      state <- csbread(csbname, sample(1:statenr, 1))
      seednr <- as.numeric(gsub("[^0-9]", "", gsub(" -.*", "", names(state)[3])))
    }

    xmin <- xlim[1]
    xmax <- xlim[2]

    runname <- paste0(gsub(".web.csb$", "", basename(csbname)), "-", seednr)
    initstate <- state[[3]]
    nstages <- ncol(state[[5]]) - nrow(state[[5]]) - 2
    initstate[,2] <- ((nrow(initstate)-1):0)
    initstate[, nstages + (16:19)] <- 0
    system(paste0("rm -f ", runname, ".*"))
    write("0.0000000", paste0(runname, ".isf"))
    write("", paste0(runname, ".isf"), append = TRUE)
    write.table(initstate, paste0(runname, ".isf"), row.names = FALSE, col.names = FALSE, sep = "\t", append = TRUE)
    write("\n\n", paste0(runname, ".isf"), append = TRUE)

    cvfbody <- rep("", 50)
    cvfbody[ 1] <- "\"Fixed step size or integration accuracy when adaptive\"                             1.0E-7"
    cvfbody[ 2] <- "\"Tolerance value, determining identity with zero\"                                   1.0E-6"
    cvfbody[ 3] <- ""
    cvfbody[ 4] <- paste0("\"Maximum integration time\"                                               ", xmax)
    cvfbody[ 5] <- "\"Output time interval:                        Only at events\"                       0.1"
    cvfbody[ 6] <- ""

    for (k in (1:28)) {
      if (k > length(state[[1]])) {
        cvfbody[6 + k] <- paste0("\"Unknown parameter\"                               ", 0.0)
      } else {
        cvfbody[6 + k] <- paste0("\"", names(state[[1]])[k], "\"                               ", state[[1]][k])
      }
    }
    writeLines(cvfbody, paste0(runname, ".cvf"))

    if (model[j] == "1s") exename <- paste0(system.file("C", package = "StructuredFoodweb"), "/LVfoodweb_1s")
    if (model[j] == "2s") exename <- paste0(system.file("C", package = "StructuredFoodweb"), "/LVfoodweb_1j1a")
    if (model[j] == "jf") exename <- paste0(system.file("C", package = "StructuredFoodweb"), "/LVfoodweb_jf")
    if (model[j] == "jfa") exename <- paste0(system.file("C", package = "StructuredFoodweb"), "/LVfoodweb_jfa")
    if (model[j] == "jfb") exename <- paste0(system.file("C", package = "StructuredFoodweb"), "/LVfoodweb_jfb")
    if (model[j] == "jfc") exename <- paste0(system.file("C", package = "StructuredFoodweb"), "/LVfoodweb_jfc")
    if (model[j] == "qna") exename <- paste0(system.file("C", package = "StructuredFoodweb"), "/LVfoodweb_qna")
    if (model[j] == "ssbm3s") exename <- paste0(system.file("C", package = "StructuredFoodweb"), "/SSBMfoodweb_2j1a")
    if ((!is.null(perturb)) && (perturb[j] >= 0.0)) {
      system(paste0(exename, " ", runname, " ", seednr, " ", perturb[j], " >", runname, ".err", " 2>&1"))
    }
    else {
      system(paste0(exename, " ", runname, " ", seednr, " >", runname, ".err", " 2>&1"))
    }

    dynmat <- read.csv(paste0(runname, ".csv"), header = FALSE)
    if (nstages == 1) {
      colnames(dynmat) <- c("Time",
                            paste0("BT", (1:basal)), paste0("T", initstate[((basal+1):nrow(initstate)),nstages+3]),
                            paste0("BA", (1:basal)), paste0("A", initstate[((basal+1):nrow(initstate)),nstages+3]))
    } else if (nstages == 2) {
      colnames(dynmat) <- c("Time",
                            paste0("BT", (1:basal)), paste0("T", initstate[((basal+1):nrow(initstate)),nstages+3]),
                            paste0("BJ", (1:basal)), paste0("J", initstate[((basal+1):nrow(initstate)),nstages+3]),
                            paste0("BA", (1:basal)), paste0("A", initstate[((basal+1):nrow(initstate)),nstages+3]))
    } else if (nstages == 3) {
      colnames(dynmat) <- c("Time",
                            paste0("BT", (1:basal)), paste0("T", initstate[((basal+1):nrow(initstate)),nstages+3]),
                            paste0("BL", (1:basal)), paste0("J", initstate[((basal+1):nrow(initstate)),nstages+3]),
                            paste0("BJ", (1:basal)), paste0("J", initstate[((basal+1):nrow(initstate)),nstages+3]),
                            paste0("BA", (1:basal)), paste0("A", initstate[((basal+1):nrow(initstate)),nstages+3]))
    }
    g[[j]] <- list(runname, seednr, dynmat)
    ### End compute the dynamics

    ### Plot the dynamics
    plotdat <- dynmat[((dynmat[,1] >= xmin) & (dynmat[,1] <= xmax)),]
    lastspeciescol <- 1 + (ncol(plotdat) - 1)/(nstages + 1)
    nonbasal <- lastspeciescol - basal

    # colors <- c("red","blue","darkgreen","darkorange","darkmagenta","gold","darkorchid","aquamarine","deeppink",
    #             "black",seq(2,991))
    # getPalette = colorRampPalette(brewer.pal(11, "Set"))
    # colors <- c("black", brewer.pal(9, "Set1"), brewer.pal(8, "Dark2"), brewer.pal(11, "Set3"))
    # qual_col_pals = brewer.pal.info[brewer.pal.info$category == 'qual',]
    # colors = c("black", unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals))))
    colors <- c("black", brewer.pal(9, "Set1")[c(1:5,7:9)], brewer.pal(8, "Dark2"))
    ncolors <- length(colors)
    colors <- rep(colors, ceiling(nonbasal/ncolors))
    colors <- c(colors[1:(nonbasal+1)])
    ltys <- c(rep(1, ncolors), rep(2, ncolors), rep(3, ncolors), rep(4, ncolors))[1:length(colors)]
    lwds <- c(rep(2, ncolors), rep(2, ncolors), rep(2, ncolors), rep(2, ncolors))[1:length(colors)]

    if (log) {
      if (is.vector(ylim) && (length(ylim) == 2)) {
        lpowy <- floor(log10(ylim[1]))
        hpowy <- ceiling(log10(ylim[2]))
        plot(plotdat[,1], plotdat[,2], type="n", xlim=c(xmin,xmax), ylim=ylim,
             log="y", xaxs="r", yaxs="r", xaxt="n", yaxt="n", xlab="", ylab="")
      } else {
        plot(plotdat[c(1,nrow(plotdat)),1],
             c(0.9*min(plotdat[,((basal+1):lastspeciescol)]), 1.1*max(plotdat[,((basal+1):lastspeciescol)])),
             type="n", xlim=c(xmin,xmax),
             log="y", xaxs="r", yaxs="r", xaxt="n", yaxt="n", xlab="", ylab="")
        lpowy <- ceiling(par("usr")[3])
        hpowy <- floor(par("usr")[4])
      }
      xleg <- 0.97*(par("usr")[2] - par("usr")[1]) + par("usr")[1]
      yleg <- 0.95*(par("usr")[4] - par("usr")[3]) + par("usr")[3]
      text(xleg, 10^yleg, paste0(gsub(".web.csb$", "", basename(csbname))), font = 2, cex = 0.9, col = "black", adj = c(1,0))
      text(xleg, 10^yleg, sprintf("%.0f", seednr), font = 2, cex = 0.9, col = "black", adj = c(1,1.25))
      if (vertical) {
        # legend(1.02*par("usr")[2], 10^(par("usr")[4]),
        #        legend=gsub("T", "", colnames(dynmat)[(basal+1):lastspeciescol]), col=colors,
        #        title=expression(bold("Species")), lty=ltys, lwd=lwds, seg.len=1.5, cex=0.8, xpd = TRUE, ncol = ceiling((nonbasal + 1)/ncolors), xjust = 0)
        legend(1.02*par("usr")[2], 10^(par("usr")[4]),
               legend=gsub("T", "", colnames(dynmat)[(basal+1):lastspeciescol]), col=colors,
               title=expression(bold("Species")), lty=ltys, lwd=lwds, seg.len=1.5, cex=0.8, xpd = TRUE, ncol = 1, xjust = 0)
      } else {
        legend(par("usr")[1], 10^(1.02*par("usr")[4]),
               legend=gsub("T", "", colnames(dynmat)[(basal+1):lastspeciescol]), col=colors,
               title=expression(bold("Species")), lty=ltys, lwd=lwds, seg.len=1.5, cex=0.8, xpd = TRUE, ncol = 5, yjust = 0)
      }
    } else {
      if (is.vector(ylim) && (length(ylim) == 2)) {
        plot(plotdat[,1], plotdat[,2], type="n", xlim=c(xmin,xmax), ylim=ylim,
             xaxs="r", yaxs="r", xaxt="n", yaxt="n", xlab="", ylab="")
      } else {
        plot(plotdat[c(1,nrow(plotdat)),1],
             c(max(0.9*min(plotdat[,((basal+1):lastspeciescol)]), 0), 1.1*max(plotdat[,((basal+1):lastspeciescol)])),
             type="n", xlim=c(xmin,xmax), xaxs="r", yaxs="r", xaxt="n", yaxt="n", xlab="", ylab="")
      }
      ymax <- as.vector(par("usr")[4])
      xleg <- 0.97*(par("usr")[2] - par("usr")[1]) + par("usr")[1]
      yleg <- 0.95*(par("usr")[4] - par("usr")[3]) + par("usr")[3]
      text(xleg, yleg, paste0(gsub(".web.csb$", "", basename(csbname))), font = 2, cex = 0.9, col = "black", adj = c(1,0))
      text(xleg, yleg, sprintf("%.0f", seednr), font = 2, cex = 0.9, col = "black", adj = c(1,1.25))
      if (vertical) {
        # legend(1.02*par("usr")[2], par("usr")[4],
        #        legend=gsub("T", "", colnames(dynmat)[(basal+1):lastspeciescol]), col=colors,
        #        title=expression(bold("Species")), lty=ltys, lwd=lwds, seg.len=1.5, cex=0.8, xpd = TRUE, ncol = ceiling((nonbasal + 1)/ncolors), xjust = 0)
        legend(1.02*par("usr")[2], par("usr")[4],
               legend=gsub("T", "", colnames(dynmat)[(basal+1):lastspeciescol]), col=colors,
               title=expression(bold("Species")), lty=ltys, lwd=lwds, seg.len=1.5, cex=0.8, xpd = TRUE, ncol = 1, xjust = 0)
      } else {
        legend(par("usr")[1], 1.02*par("usr")[4],
               legend=gsub("T", "", colnames(dynmat)[(basal+1):lastspeciescol]), col=colors,
               title=expression(bold("Species")), lty=ltys, lwd=lwds, seg.len=1.5, cex=0.8, xpd = TRUE, ncol = 5, yjust = 0)
      }
    }
    for (i in ((basal+1):lastspeciescol)) {
      lines(plotdat[,1], plotdat[,i], col=colors[i-1], lty = ltys[i-1], lwd = lwds[i-1])
    }

    ### End plot the dynamics

    ### Decorate the axes
    if ((!vertical) || (j == nfiles))
      axis(1, labels = TRUE, cex.axis = 1.1)
    else
      axis(1, labels = FALSE, cex.axis = 1.1)
    if ((j == 1) || vertical) {
      if (log) {
        ymax <- 10^(as.vector(par("usr")[4]))
        ylbls <- c(expression(10^{-9}), expression(10^{-8}), expression(10^{-7}), expression(10^{-6}))
        ylbls <- c(ylbls, expression(10^{-5}), expression(10^{-4}), expression(10^{-3}))
        ylbls <- c(ylbls, "0.01", "0.1", "1.0", "10", "100")
        ylbls <- c(ylbls, expression(10^{3}), expression(10^{4}), expression(10^{5}), expression(10^{6}))
        ylbls <- c(ylbls, expression(10^{7}), expression(10^{8}), expression(10^{9}))
        axis(side=2, at=c(10^(lpowy:hpowy)), labels=ylbls[(10+lpowy):(10+hpowy)], cex.axis=1.1, las=2)
      } else {
        axis(side=2, cex.axis=1.1, las=2)
      }
    } else {
      if (log) {
        axis(side=2, at=c(10^(lpowy:hpowy)), labels=F, cex.axis=1.1, las=2)
      } else {
        axis(2, labels = F, cex.axis = 1.1)
      }
    }

    if (vertical) {
      if (j == nfiles) mtext(side = 1, text = "Time", line = 3.5, cex = 1.5)
    } else {
      if ((j == (nfiles + 2 - (nfiles %% 2))/2)) {
        if ((nfiles %% 2) == 1)
          mtext(side = 1, text = "Time", line = 3.5, cex = 1.5)
        else
          mtext(side = 1, text = "Time", line = 3.5, cex = 1.5, at = par("usr")[1])
      }
    }
    if ((j == 1) || vertical) mtext(side = 2, text = "Density", line = 3.5, las = 0, cex = 1.5)
    ### End decorating the axes
  }

  setwd(oldwd)
  par(def.par)  #- reset to default
  return(g)
}

