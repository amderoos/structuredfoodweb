/***
   NAME
     io.c
   PURPOSE
     I/O routines for stand-alone foodweb program. 
     
   Last modification: AMdR - Jan 30, 2023
***/

#include "foodweb.h"

static int                        UserNotesNr = 0;

//==================================================================================
/*
 * Start of function implementations.
 */
//==================================================================================

static char *ReadDouble(double *val, char *cpnt)

/* 
   * ReadDouble - Routine reads a double value from the string pointed to
   *      by "cpnt". Invalid characters are skipped. It returns a 
   *      pointer to the rest of the string or NULL on error.
   */

{
  char *ch, *end = NULL;
  int dot_start = 0;

  ch = cpnt;
  while ((!isdigit(*ch)) && *ch) ch++;                                              // Skip non digits
  if (isdigit(*ch))
    {
      end = ch;
      if ((ch != cpnt) && (*(ch - 1) == '.'))                                       // Is previous a dot?
        {
          ch--;
          dot_start = 1;
        }
      if ((ch != cpnt) && (*(ch - 1) == '-')) ch--;                                 // Is previous a minus?

      while (isdigit(*end)) end++;                                                  // Skip digits
      if ((!dot_start) && (*end == '.'))                                            // Dot starts mantissa
        {
          end++;
          while (isdigit(*end))
            end++;
        }

      if ((*end == 'e') || (*end == 'E'))                                           // Possible exponent
        {
          end++;
          if ((*end == '+') || (*end == '-'))
            end++;
          while (isdigit(*end))
            end++;
        }
      *val = 0.0;
      (void)sscanf(ch, "%lg", val);
    }

  return end;
}

//==================================================================================

static int ScanLineDouble(FILE *infile, char *descr, double *value, int var_nr)

/* 
   * ScanLineDouble - Routine reads one line from the control variable file, 
   *          splits it up in the description and the value part and 
   *          returns the number of values read. "var_nr" is the number
   *          of variables to be read.
   */

{
  char                            *ch = NULL, *dsp;
  char                            input[MAX_INPUT_LINE], tmp_str[MAX_INPUT_LINE];
  int                             read_no = 0, desclen = 0;

  // Input a single line. Skip empty lines and lines starting with a hatch (#) sign
  while ((!feof(infile)) && (!ferror(infile)) && (!ch))
    {
      if ((ch = fgets(input, MAX_INPUT_LINE, infile)))
        {
          while (*ch == ' ')
            ch++;
          if ((*ch == '#') || (*ch == '\n'))
            ch = NULL;
        }
    }
  if (feof(infile) || ferror(infile))
    ProgramExit(2, "Unexpected end/error while reading CVF file!");

  dsp = descr;
  if (*ch == '"')                                                                   // Comment is in quotes
    {
      ch++;                                                                         // Skip the first quote
      while (*ch != '"')                                                            // Search for the closing quotes, storing string
        {                                                                           // between them
          if (desclen < DESCRIP_MAX)
            (*dsp) = (*ch);
          dsp++;
          ch++;
          desclen++;
        }
      ch++;                                                                         // Skip closing quotes
    }
  else                                                                              // Comment is not quoted
    {
      while ((!isdigit(*ch)) && *ch)                                                // Skip non-digits and put them in description
        {
          if (desclen < DESCRIP_MAX) (*dsp) = (*ch);
          dsp++;
          ch++;
          desclen++;
        }                                                                           // Remove trailing blanks and tabs
      while ((*(dsp - 1) == ' ') || (*(dsp - 1) == '\t'))
        *(--dsp) = '\0';
    }
  descr[DESCRIP_MAX - 1] = '\0';

  ch = strcpy(tmp_str, ch);                                                         // Copy rest to string

  while (ch)
    if ((ch = ReadDouble(value + read_no, ch)) != NULL)
      if ((++read_no) == var_nr)
        return read_no;

  return read_no;
}

//==================================================================================

static void ReadCvf(FILE *infile)

/* 
   * ReadCvf - Routine reads all the values of the control variables from 
   *         the already opened .cvf file.
   */

{
  int                             jj, read_no;
  char                            msg[MAX_INPUT_LINE];

  read_no = ScanLineDouble(infile, description.Accuracy, &Accuracy, 1);
  if (read_no != 1)
    ProgramExit(2, "Error during input of Accuracy value from CVF file!");
  if (Accuracy < MIN_ACCURACY)
    ProgramExit(2, "Relative Accuracy required too small. Use Accuracy > 1.0E-12!");

  read_no=ScanLineDouble(infile, description.IdenticalZero, &IdenticalZero, 1);
  if(read_no != 1) 
    ProgramExit(2, "Error during input of zero comparison value from CVF file!");

  read_no = ScanLineDouble(infile, description.MaxTime, &MaxTime, 1);
  if (read_no != 1)
    ProgramExit(2, "Error during input of maximum integration time from CVF file!");

  read_no = ScanLineDouble(infile, description.OutputInterval, &OutputInterval, 1);
  if (read_no != 1)
    ProgramExit(2, "Error during reading of output time interval from CVF file!");

  for (jj = 0; jj < PARAMETER_NR; jj++)
    {
      read_no = ScanLineDouble(infile, description.parameter[jj], parameter + jj, 1);
      if (read_no != 1)
        {
          (void)sprintf(msg, "Error while reading parameter %d from CVF file!", jj);
          ProgramExit(2, msg);
        }
    }

  // Ensure parameter sanity
  ALPHA_WIDTH = dmax(ALPHA_WIDTH,  0.0);
  ALPHA_WIDTH = dmin(ALPHA_WIDTH,  1.0);

  FHRES_WIDTH = dmax(FHRES_WIDTH,  0.0);
  FHRES_WIDTH = dmin(FHRES_WIDTH,  1.0);

  MAINT_WIDTH = dmax(MAINT_WIDTH,  0.0);
  MAINT_WIDTH = dmin(MAINT_WIDTH,  1.0);

  GAMMA_WIDTH = dmax(GAMMA_WIDTH,  0.0);
  GAMMA_WIDTH = dmin(GAMMA_WIDTH,  1.0);

  MORT_WIDTH  = dmax(MORT_WIDTH,   0.0);
  MORT_WIDTH  = dmin(MORT_WIDTH,   1.0);

  JUVADUSPEC_WIDTH = dmin(JUVADUSPEC_WIDTH, JUVADUSPEC_CONST);
  JUVADUSPEC_WIDTH = dmin(JUVADUSPEC_WIDTH, 2.0 - JUVADUSPEC_CONST);

#if (STAGES == 1)
  QALPHA = 1.0;
  QMAINT = 1.0;
  QGAMMA = 1.0;
  QMORT  = 1.0;

  JUVADUSPEC_CONST = 1.0;
  JUVADUSPEC_WIDTH = 0.0;
#else
  QALPHA = dmax(QALPHA, 0.0);
  QMAINT = dmax(QMAINT, 0.0);
  QGAMMA = dmax(QGAMMA, 0.0);
  QMORT  = dmax(QMORT , 0.0);

  QALPHA = dmin(QALPHA, 2.0);
  QMAINT = dmin(QMAINT, 2.0);
  QGAMMA = dmin(QGAMMA, 2.0);
  QMORT  = dmin(QMORT , 2.0);
#endif

  return;
}

//==================================================================================

static void ReadInitialState(FILE *infile)

  /* 
   * ReadInitialState - Routine reads the initial values for the state of the foodweb 
   */

{
  int                             jj;
  char                            *ch, input[MAX_INPUT_LINE];
  int                             done, read_no, warnics = 1;
  double                          val_tmp[SPECIESINPUTLEN];

  done = 0;                                                                         // Flag indicating end of input data
  while ((!feof(infile)) && (!ferror(infile)) && (!done))
    {
      ch = fgets(input, MAX_INPUT_LINE, infile);                                    // Input line

      // Initialize all data to default: MISSING_VALUE
      for (jj = 0; jj < SPECIESINPUTLEN; jj++) val_tmp[jj] = MISSING_VALUE;

      // Read complete species data from one line. Stop on line end or full input line
      for (jj = 0, read_no = 0; (jj < SPECIESINPUTLEN) && (ch); jj++)
        if ((ch = ReadDouble(val_tmp + jj, ch)) != NULL) read_no++;

      // Empty line encountered: Signals end of species input data if species data are already present
      if ((read_no == 0) && Species) done = 1;
      else if (read_no > 0)
        {      
          if (read_no >= SPECIESINPUTLEN)                                           // Store species data 
            {
              jj = 0;

              if (Species >= MAX_SPECIES)
                ProgramExit(1, "Number of species in ISF is larger than MAX_SPECIES.\nRecompile the program with appropriate MAX_SPECIES value!");

              TotDens[Species]         = val_tmp[jj++];
              SpeciesAge[Species]      = 0; jj++;                                   // Skip the species age value
#if (STAGES > 1)
              JuvDens[Species]         = val_tmp[jj++];
#if (STAGES> 2)
              ImmDens[Species]         = val_tmp[jj++];
#endif
#endif
              AduDens[Species]         = val_tmp[jj++];

              SpeciesID[Species]       = (int)lround(val_tmp[jj++]);

              NicheVal[Species]        = val_tmp[jj++];
              SpeciesBodySize[Species] = val_tmp[jj++];

              Alpha[Species]           = val_tmp[jj++];
              Fhres[Species]           = val_tmp[jj++];
              JuvAduSpec[Species]      = val_tmp[jj++];
              Gamma[Species]           = val_tmp[jj++];
              Maint[Species]           = val_tmp[jj++];
              Mort[Species]            = val_tmp[jj++];

              MinFoodNiche[Species]    = val_tmp[jj++];
              MaxFoodNiche[Species]    = val_tmp[jj++];
              Species++;
            }
          else if (warnics)                                                         // Warn of incomplete species specification
            {
              Warning("Incomplete species specification(s) encountered in ISF file!");
              warnics = 0;
            }
        }
    }
  if (ferror(infile) || (feof(infile) && !Species))
    Warning("Unexpected end/error while reading the initial state file!");          // On read error exit

  return;
}

//==================================================================================

void ReportNote(const char *fmt, ...)

{
  va_list                         argpnt;

  va_start(argpnt, fmt);
  vsnprintf(usernotes[UserNotesNr], REPORTNOTE_MAXLEN, fmt, argpnt);
  va_end(argpnt);

  UserNotesNr++;

  return;
}

//==================================================================================

void Warning(char *mes)

  /*
   * Warning - Routine issues a warning message pertaining to the current
   *	       state of the program. The program continues normally.
   */

{
  (void)fprintf(stderr, "\nRUN %-s: WARNING at T = %.2f:\n", runname, CurrentTime);
  (void)fprintf(stderr, "** %-70s **\n", mes);
  (void)fprintf(stderr, "** %-70s **\n\n", "Program continues normally.");

  return;
}

//==================================================================================

void	  PrettyPrint(FILE *fp, double value)

/*
 * PrettyPrint - Formatted print of output to the file pointed to by fp.
 */

{
  int                             fpclass = fpclassify(value);

  if (fpclass == FP_NORMAL)
    {
      if (((fabs(value) <= 1.0E4) && (fabs(value) >= 1.0E-4)) || (value == 0))
	      (void)fprintf(fp, "%.10f", value);
      else
	      (void)fprintf(fp, "%.6E", value);
    }
  else (void)fprintf(fp, "%E", value);

  return;
}

//==================================================================================

void	CatchSig(int signl)

  /*
   * CatchSig - Routine catches and handles the signals received from the
   *            ebttool application running this program as a child.
   */

{
  switch (signl)
    {
#if (!defined(_WIN32) && !defined(_WIN64))
    case SIGHUP:                                                                    // End integration
      ProgramExit(1, "Hangup enforced by user signal");
      break;
#endif
    case SIGFPE:                                                                    // Floating point exception
      ProgramExit(1, "Floating point error");
      break;
    }

#if (!defined(_WIN32) && !defined(_WIN64))
  if (signal(SIGHUP, CatchSig) == SIG_ERR) Warning("Error in installing the signal handlers!");
#endif
  if (signal(SIGFPE, CatchSig) == SIG_ERR) Warning("Error in installing the signal handlers!");

  return;
}

//==================================================================================

void ProgramExit(int exitcode, char *mes)

  /*
   * ProgramExit - Routine issues an error message and tries to call the
   *		 ShutDown() routine to save the state of the system. The
   *		 program subsequently terminates.
   */

{
  int                             ii;
  char                            filename[MAXPATHLEN];

  if (exitcode > 1)
    {
      (void)fprintf(stderr, "\nRUN %-s: ERROR at T = %.2f:\n", runname, CurrentTime);
      (void)fprintf(stderr, "** %-70s **\n", mes);
    }

  if ((exitcode == 0) || (exitcode == 1))
    {
      FILE                        *esf;

			// Save final state. Open ESF file with lower case extension
      (void)strcpy(filename, runname);
      (void)strcat(filename, ".esf");
      esf = fopen(filename, "w");

      // Write the current time
      PrettyPrint(esf, CurrentTime);
      (void)fprintf(esf, "\n\n");

      // Write all the species data
      for (ii = 0; ii < Species; ii++)
        {
          PrettyPrint(esf, TotDens[ii]);              (void)fprintf(esf, "\t");
          PrettyPrint(esf, SpeciesAge[ii]);           (void)fprintf(esf, "\t");
#if (STAGES > 1)
          PrettyPrint(esf, JuvDens[ii]);              (void)fprintf(esf, "\t");
#if (STAGES > 2)
          PrettyPrint(esf, ImmDens[ii]);              (void)fprintf(esf, "\t");
#endif
#endif
          PrettyPrint(esf, AduDens[ii]);              (void)fprintf(esf, "\t");
          (void)fprintf(esf, "%10d", SpeciesID[ii]);  (void)fprintf(esf, "\t");

          PrettyPrint(esf, NicheVal[ii]);             (void)fprintf(esf, "\t");
          PrettyPrint(esf, SpeciesBodySize[ii]);      (void)fprintf(esf, "\t");

          PrettyPrint(esf, Alpha[ii]);                (void)fprintf(esf, "\t");
          PrettyPrint(esf, Fhres[ii]);                (void)fprintf(esf, "\t");
          PrettyPrint(esf, JuvAduSpec[ii]);           (void)fprintf(esf, "\t");
          PrettyPrint(esf, Gamma[ii]);                (void)fprintf(esf, "\t");
          PrettyPrint(esf, Maint[ii]);                (void)fprintf(esf, "\t");
          PrettyPrint(esf, Mort[ii]);                 (void)fprintf(esf, "\t");

          PrettyPrint(esf, MinFoodNiche[ii]);         (void)fprintf(esf, "\t");
          PrettyPrint(esf, MaxFoodNiche[ii]);         (void)fprintf(esf, "\t");

          (void)fprintf(esf, "%10d", PreyNr[ii]);     (void)fprintf(esf, "\t");
          (void)fprintf(esf, "%10d", PredNr[ii]);     (void)fprintf(esf, "\t");

          PrettyPrint(esf, TotDensMean[ii]);          (void)fprintf(esf, "\t");
          PrettyPrint(esf, TotDensVariance[ii]);      (void)fprintf(esf, "\t");
          PrettyPrint(esf, TotDensMin[ii]);           (void)fprintf(esf, "\t");
          PrettyPrint(esf, TotDensMax[ii]);           (void)fprintf(esf, "\t");
#if (STAGES > 1)
          PrettyPrint(esf, JuvDensMean[ii]);          (void)fprintf(esf, "\t");
          PrettyPrint(esf, JuvDensVariance[ii]);      (void)fprintf(esf, "\t");
          PrettyPrint(esf, JuvDensMin[ii]);           (void)fprintf(esf, "\t");
          PrettyPrint(esf, JuvDensMax[ii]);           (void)fprintf(esf, "\t");
#if (STAGES > 2)
          PrettyPrint(esf, ImmDensMean[ii]);          (void)fprintf(esf, "\t");
          PrettyPrint(esf, ImmDensVariance[ii]);      (void)fprintf(esf, "\t");
          PrettyPrint(esf, ImmDensMin[ii]);           (void)fprintf(esf, "\t");
          PrettyPrint(esf, ImmDensMax[ii]);           (void)fprintf(esf, "\t");
#endif
#endif
          PrettyPrint(esf, AduDensMean[ii]);          (void)fprintf(esf, "\t");
          PrettyPrint(esf, AduDensVariance[ii]);      (void)fprintf(esf, "\t");
          PrettyPrint(esf, AduDensMin[ii]);           (void)fprintf(esf, "\t");
          PrettyPrint(esf, AduDensMax[ii]);           (void)fprintf(esf, "\n");
        }
      (void)fprintf(esf, "\n");
      (void)fclose(esf);                                                            // Close end state file

      if (exitcode == 1)
        {
          (void)fprintf(stderr, "\nRUN %-s: ERROR at T = %.2f:\n", runname, CurrentTime);
          (void)fprintf(stderr, "** %-70s **\n", mes);
        }
      else
        {
          (void)fprintf(stderr, "\n\nRUN %-s COMPLETED at T = %.2f:\n", runname, CurrentTime);
          (void)fprintf(stderr, "** %-70s **\n\n", "Program terminated. Normal closure of output files succeeded.");
        }
      (void)fflush(stdout); 
      (void)fflush(stderr);
    }

#ifdef DEBUG
  if (exitcode) abort();
#endif  /* DEBUG */
  exit(exitcode);

  return;
}

//==================================================================================

void WriteReport(int argc, char **argv)

{
  int		                          ii;
  char			                      filename[MAXPATHLEN];
  FILE			                      *rep;

	// Open REP file with	lower case extension
  strcpy(filename, runname);
  strcat(filename, ".rep");
  rep = fopen(filename, "w");
  
  for(ii = 0; ii < 80; ii++) (void)fprintf(rep, "*");
  (void)fprintf(rep, "\n");
  (void)fprintf(rep, "\n%2s%-s\n", " ", "PROGRAM, RUN AND ARGUMENTS");
  (void)fprintf(rep, "%4sProgram   : %-s\n", " ", progname);
  (void)fprintf(rep, "%4sRun       : %-s\n", " ", runname);
  (void)fprintf(rep, "%4sArguments : ", " ");
  for (ii = 2; ii < argc; ii++) (void)fprintf(rep, " %-s", argv[ii]);
  (void)fprintf(rep, "\n");

  if (UserNotesNr)
    {
      (void)fprintf(rep, "\n%2s%-s\n", " ", "MODEL SPECIFIC NOTES");
      for (ii = 0; ii < UserNotesNr; ii++) (void)fprintf(rep, "%4s%-s\n", " ", usernotes[ii]);
    }
  
  (void)fprintf(rep, "\n%2s%-s\n", " ", "USED VALUES FOR CONTROL VARIABLES");
  (void)fprintf(rep, "%4s%-65s%5s%-10.4G\n", " ", description.Accuracy, "  :  ", Accuracy);
  (void)fprintf(rep, "%4s%-65s%5s%-10.4G\n", " ", description.IdenticalZero, "  :  ", IdenticalZero);
  (void)fprintf(rep, "%4s%-65s%5s%-10.2f\n", " ", description.MaxTime, "  :  ", MaxTime);
  (void)fprintf(rep, "%4s%-65s%5s%-10.2f\n", " ", description.OutputInterval, "  :  ", OutputInterval);

  (void)fprintf(rep, "\n%2s%-s\n", " ", "USED VALUES OF PARAMETERS");
  
  for(ii = 0; ii < PARAMETER_NR; ii++)
    (void)fprintf(rep, "%4s%-65s%5s%-10.4G\n", " ", description.parameter[ii], "  :  ", parameter[ii]);

  for(ii = 0; ii < 80; ii++) (void)fprintf(rep, "*");
  (void)fprintf(rep, "\n");
  
  (void)fclose(rep);
  
  return;
}

//==================================================================================

void Initialize(int argc, char **argv)

  /* 
   * Initialize - Routine initializes the global variables, reads the 
   *              constants from the .cvf file and the initial state from
   *              the .isf file if present
   */

{
  char                            filename[MAXPATHLEN];
  FILE                            *isf, *cvf;

  if (argc < 2) ProgramExit(2, "Not enough arguments : Usage '<program name> <run name>'");

  strcpy(progname, argv[0]);                                                        // Store name of the executable
  strcpy(runname,  argv[1]);                                                        // Store name of the run
  
  CurrentTime = 0.0;
  Species = 0;

  memset(SpeciesIntData, 0, (SPECIESINTLEN*MAX_SPECIES) * sizeof(int));
  memset(SpeciesDblData, 0, (SPECIESDBLLEN*MAX_SPECIES) * sizeof(double));

  RearrangeMem(0, MAX_SPECIES);

  strcpy(filename, runname);                                                        // Open CVF file with lower case extension
  strcat(filename, ".cvf");
  cvf = fopen(filename, "r");

  if(!cvf)                                                                          // On error try upper casee
    {
      strcpy(filename, runname);
      strcat(filename, ".CVF");
      cvf = fopen(filename, "r");
      if(!cvf)
        ProgramExit(2, "Unable to open CVF file with control variables for program!");// On repeated error exit
    }
  ReadCvf(cvf); (void)fclose(cvf);

  // Initiate the random number generator
  if (argc > 2) UNIDEVSEED = atof(argv[2]);                                         // Random number seed givne on command line
  if (UNIDEVSEED > 0.0) UniDevSeed = (long)UNIDEVSEED;
  (void)UniDev();

  strcpy(filename, runname);                                                        // Open ISF file with lower case extension
  strcat(filename, ".isf");
  isf = fopen(filename, "r");
  if(!isf)                                                                          // On error try upper case
    {
      strcpy(filename, runname);
      strcat(filename, ".ISF");
      isf = fopen(filename, "r");
    }
  if(isf)
    {
      ReadInitialState(isf);                                                        // Read initial state
      (void)fclose(isf);
    }
  else
    Warning("No ISF file found! Random foodweb will be generated");

  if (!Species) 
    {
      DynamicsRun   = 0;
      BuildWeb();
      ConstructInteractionMatrix();
    }
  else
    {
      DynamicsRun   = 1;
      Species = RearrangeMem(MAX_SPECIES, Species);
      ConstructInteractionMatrix();
    }

#if (!defined(_WIN32) && !defined(_WIN64))
  if (signal(SIGHUP,  CatchSig) == SIG_ERR) Warning("Error in installing the signal handlers!");
#endif
  if (signal(SIGFPE,  CatchSig) == SIG_ERR) Warning("Error in installing the signal handlers!");

  return;
}

//==================================================================================
/*
 * WebToCsbFile - Routine writes the entire network of interactions in the
 *      to a CSB file, which can be read by the EBTtool or by the routine
 *      csbclean() from the R package "PSPManalysis".
 * 
 *      The information is written as 2 separate matrices. Each row represents
 *      a species. 
 *      The first 1 or 2 cohort variables are the densities of the
 *      juvenile and adult stage, the third is the species body size.
 *
 *      What follows are (Species) numbers that represent the
 *      interactions with all stages of all species in the community
 *      (hence including itself and other stages of the species it
 *      belongs to). These values are either 0, if the particular
 *      species stage does not have an interaction with the other
 *      species stage. Alternatively, the value equals the body size
 *      of the other species stage that it hence feeds on.
 *
 *      The second population has the same structure exactly, but
 *      instead of the body size of the species the particular
 *      species stage feeds on, the interaction strength is saved
 *      (Phi[][])
 */

typedef struct envdim {
                       double     timeval;
                       int        columns;
                       int        data_offset;
                       uint32_t   memory_used;
                      }           Envdim;

typedef struct popdim {
                       double     timeval;
                       int        population;
                       int        cohorts;
                       int        columns;
                       int        data_offset;
                       int        lastpopdim;
                      }           Popdim;

void WebToCsbFile()

{
  int                             prey, pred;
  size_t                          cenvdbls, penvdbls, lbldbls;
  double                          zero  = 0.0, tmpdbl;
  char                            filename[MAXPATHLEN];
  char                            label[REPORTNOTE_MAXLEN];
  Envdim                          cenv[2];
  Popdim                          cpop[2];
  static int                      first = 1;
  static FILE                     *webfil = NULL;

  // On first call open the CSB file for output
  if (first)
    {
      const uint32_t              CSB_MAGIC_KEY = 20030509;
      uint32_t                    tmpint32;
      int                         tmpint;
      struct stat                 st;

      (void)strcpy(filename, runname);
      (void)strcat(filename, ".web.csb");
      if ((stat(filename, &st) != 0) || (st.st_size == 0))
        {
          webfil = fopen(filename, "wb");                                           // New CSB file

          // New CSB file: Write magic key and parameters
          tmpint32 = CSB_MAGIC_KEY;
          fwrite((void *)(&tmpint32), 1, sizeof(uint32_t), webfil);
          tmpint = PARAMETER_NR;
          fwrite((void *)(&tmpint), 1, sizeof(int), webfil);
          fwrite((void *)(&parameter), PARAMETER_NR, sizeof(double), webfil);
        }
      else
        webfil = fopen(filename, "ab");
    }
  first = 0;

  sprintf(label, "Random-number seed %ld - %d-species web at T=%.0f", UniDevSeed, Species, CurrentTime);

  cenvdbls = (sizeof(Envdim)/sizeof(double)) + 1;
  penvdbls = (sizeof(Popdim)/sizeof(double)) + 1;
  lbldbls  = (strlen(label)*sizeof(char))/sizeof(double) + 1;

  (void)memset((void *)cenv, 0, cenvdbls*sizeof(double));
  cenv->timeval      = (double)UniDevSeed;
  cenv->columns      = 1;
  cenv->data_offset  = cenvdbls;
  cenv->memory_used  = cenvdbls*sizeof(double);
  cenv->memory_used += sizeof(double);

  // First population:
  //
  // Rows are species, columns are all variables in the species data matrix
  // Do not store the species column in the output and the 3 additional variables
  // used in the juvenile fraction model
  //
  cenv->memory_used += (penvdbls + lbldbls)*sizeof(double);
  cenv->memory_used += (Species*(SPECIESDBLLEN + SPECIESINTLEN - 1 - 3 * JFRAC)*sizeof(double));

  // Second population:
  //
  // Rows are species, columns are:
  // (1)      Species ID
  // (2)      Predator body weight
  // (3-4)    Stage-specific density(ies)
  // (5)->(N) Body weights of all prey species
  //
  cenv->memory_used += (penvdbls + lbldbls)*sizeof(double);
  cenv->memory_used += (Species*(2 + STAGES + Species)*sizeof(double));

  // Third population:
  //
  // Rows are species, columns are:
  // (1)      Species ID
  // (2)      Predator body weight
  // (3-4)    Stage-specific density(ies)
  // (5)->(N) Species ID of all prey species
  //
  cenv->memory_used += (penvdbls + lbldbls)*sizeof(double);
  cenv->memory_used += (Species*(2 + STAGES + Species)*sizeof(double));

  fwrite((void *)cenv, 1, cenvdbls*sizeof(double), webfil);
  fwrite((void *)(&CurrentTime), 1, sizeof(double), webfil);

  // First population
  (void)memset((void *)cpop, 0, penvdbls*sizeof(double));
  cpop->timeval     = (double)UniDevSeed;
  cpop->population  = 0;
  cpop->columns     = SPECIESDBLLEN + SPECIESINTLEN - 1 - 3 * JFRAC;
  cpop->cohorts     = Species;
  cpop->data_offset = penvdbls + lbldbls;
  cpop->lastpopdim  = 0;

  fwrite((void *)cpop,  1, penvdbls*sizeof(double), webfil);
  fwrite((void *)label, 1, lbldbls*sizeof(double),  webfil);

  fwrite((void *)TotDens,         sizeof(double), Species, webfil);
  fwrite((void *)SpeciesAge,      sizeof(double), Species, webfil);
#if (STAGES > 1)
  fwrite((void *)JuvDens,         sizeof(double), Species, webfil);
#if (STAGES > 2)
  fwrite((void *)ImmDens,         sizeof(double), Species, webfil);
#endif
#endif
  fwrite((void *)AduDens,         sizeof(double), Species, webfil);

  for (pred = 0; pred < Species; pred++)
    {
      tmpdbl = (double)SpeciesID[pred];
      fwrite((void *)(&tmpdbl),   sizeof(double), 1, webfil);
    }

  fwrite((void *)NicheVal,        sizeof(double), Species, webfil);
  fwrite((void *)SpeciesBodySize, sizeof(double), Species, webfil);

  fwrite((void *)Alpha,           sizeof(double), Species, webfil);
  fwrite((void *)Fhres,           sizeof(double), Species, webfil);
  fwrite((void *)JuvAduSpec,      sizeof(double), Species, webfil);
  fwrite((void *)Gamma,           sizeof(double), Species, webfil);
  fwrite((void *)Maint,           sizeof(double), Species, webfil);
  fwrite((void *)Mort,            sizeof(double), Species, webfil);

  fwrite((void *)MinFoodNiche,    sizeof(double), Species, webfil);
  fwrite((void *)MaxFoodNiche,    sizeof(double), Species, webfil);

  for (pred = 0; pred < Species; pred++)
    {
      tmpdbl = (double)PreyNr[pred];
      fwrite((void *)(&tmpdbl),   sizeof(double), 1, webfil);
    }
  for (pred = 0; pred < Species; pred++)
    {
      tmpdbl = (double)PredNr[pred];
      fwrite((void *)(&tmpdbl),   sizeof(double), 1, webfil);
    }

  fwrite((void *)TotDensMean,     sizeof(double), Species, webfil);
  fwrite((void *)TotDensVariance, sizeof(double), Species, webfil);
  fwrite((void *)TotDensMin,      sizeof(double), Species, webfil);
  fwrite((void *)TotDensMax,      sizeof(double), Species, webfil);
#if (STAGES > 1)
  fwrite((void *)JuvDensMean,     sizeof(double), Species, webfil);
  fwrite((void *)JuvDensVariance, sizeof(double), Species, webfil);
  fwrite((void *)JuvDensMin,      sizeof(double), Species, webfil);
  fwrite((void *)JuvDensMax,      sizeof(double), Species, webfil);
#if (STAGES > 2)
  fwrite((void *)ImmDensMean,     sizeof(double), Species, webfil);
  fwrite((void *)ImmDensVariance, sizeof(double), Species, webfil);
  fwrite((void *)ImmDensMin,      sizeof(double), Species, webfil);
  fwrite((void *)ImmDensMax,      sizeof(double), Species, webfil);
#endif
#endif
  fwrite((void *)AduDensMean,     sizeof(double), Species, webfil);
  fwrite((void *)AduDensVariance, sizeof(double), Species, webfil);
  fwrite((void *)AduDensMin,      sizeof(double), Species, webfil);
  fwrite((void *)AduDensMax,      sizeof(double), Species, webfil);

  // Second population
  (void)memset((void *)cpop, 0, penvdbls*sizeof(double));
  cpop->timeval     = (double)UniDevSeed;
  cpop->population  = 1;
  cpop->columns     = 2 + STAGES + Species;
  cpop->cohorts     = Species;
  cpop->data_offset = penvdbls + lbldbls;
  cpop->lastpopdim  = 0;

  fwrite((void *)cpop, 1, penvdbls*sizeof(double), webfil);
  fwrite((void *)label, 1, lbldbls*sizeof(double), webfil);

  // Write the species ID
  for (pred = 0; pred < Species; pred++)
    {
      tmpdbl = (double)SpeciesID[pred];
      fwrite((void *)(&tmpdbl),   sizeof(double), 1, webfil);
    }

  // Write the species body sizes
  fwrite((void *)SpeciesBodySize, sizeof(double), Species, webfil);

  // Write the species stage densities
#if (STAGES > 1)
  fwrite((void *)JuvDens,         sizeof(double), Species, webfil);
#if (STAGES > 2)
  fwrite((void *)ImmDens,         sizeof(double), Species, webfil);
#endif
#endif
  fwrite((void *)AduDens,         sizeof(double), Species, webfil);

  // Write the body weights of all prey species
  for (prey = 0; prey < Species; prey++)
    for (pred = 0; pred < Species; pred++)
      if (Phi[pred][prey] > MACH_PREC)
        fwrite((void *)(SpeciesBodySize + prey), sizeof(double), 1, webfil);
      else
        fwrite((void *)&zero, sizeof(double), 1, webfil);

  // Third population
  (void)memset((void *)cpop, 0, penvdbls*sizeof(double));
  cpop->timeval     = (double)UniDevSeed;
  cpop->population  = 2;
  cpop->columns     = 2 + STAGES + Species;
  cpop->cohorts     = Species;
  cpop->data_offset = penvdbls + lbldbls;
  cpop->lastpopdim  = 1;

  fwrite((void *)cpop, 1, penvdbls*sizeof(double), webfil);
  fwrite((void *)label, 1, lbldbls*sizeof(double), webfil);

  // Write the species ID
  for (pred = 0; pred < Species; pred++)
    {
      tmpdbl = (double)SpeciesID[pred];
      fwrite((void *)(&tmpdbl),   sizeof(double), 1, webfil);
    }

  // Write the species body sizes
  fwrite((void *)SpeciesBodySize, sizeof(double), Species, webfil);

  // Write the species stage densities
#if (STAGES > 1)
  fwrite((void *)JuvDens,         sizeof(double), Species, webfil);
#if (STAGES > 2)
  fwrite((void *)ImmDens,         sizeof(double), Species, webfil);
#endif
#endif
  fwrite((void *)AduDens,         sizeof(double), Species, webfil);

  // Write the IDs of all prey species
  for (prey = 0; prey < Species; prey++)
    for (pred = 0; pred < Species; pred++)
      {
        if (Phi[pred][prey] > MACH_PREC)
          {
            tmpdbl = (double)SpeciesID[prey];
            fwrite((void *)(&tmpdbl), sizeof(double), 1, webfil);
          }
        else
          fwrite((void *)&zero, sizeof(double), 1, webfil);
      }

  (void)fflush(webfil);                                                             // Flush the file buffer

  return;
}

//==================================================================================


