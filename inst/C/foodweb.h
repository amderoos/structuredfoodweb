/***
  NAME
    foodweb.h
  PURPOSE
    Header file for the foodweb model.
 
   Last modification: AMdR - Nov 03, 2020
***/

#include "float.h"
#include <ctype.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <signal.h>
#include <time.h>


//==================================================================================
/*
 * Definition of global constants
 */

#ifdef FOODWEB_C
#undef EXTERN
#define EXTERN
#else
#undef EXTERN
#define EXTERN extern
#endif

#define DESCRIP_MAX               80                                                // The maximum length of a quantity description   
#define MAX_INPUT_LINE            2048                                              // Maximum lenght of input line
#define MIN_ACCURACY              1.0E-16                                           // The minimum accuracy allowed in integration   
#define REPORTNOTE_MAXNR          30                                                // The maximum number of ReportNotes
#define REPORTNOTE_MAXLEN         4096                                              // The maximum length of a ReportNote
#define MISSING_VALUE             DBL_MAX
#define MACH_PREC                 1.0E-15
#define SMALLEST_STEP             1.0E-12
#define LARGEST_STEP              10.0

#ifndef MAX_SPECIES
#define MAX_SPECIES               200                                               // Default: 500
#endif

#ifndef STAGES
#define STAGES                    2
#endif

#if (!defined(JFRAC) || (JFRAC != 1) || (SSBM == 1))
#undef JFRAC
#define JFRAC                     0                                                 // Juvenile fraction model only iff -DJFRAC=1 
#endif

// -DCONSTRATES=1 implies constant maturation rate (= age-dependent maturation)
// -DCONSTRATES=2 implies constant fecundity
#if (!defined(CONSTRATES) || (CONSTRATES < 1) || (CONSTRATES > 2))
#undef CONSTRATES
#define CONSTRATES                0
#endif

// -DQNA=1 implies quasi neutral approximation of the structured model
#if (defined(QNA) && (QNA == 1))
#undef SSBM
#define SSBM                      0
EXTERN double                     ReproVal[MAX_SPECIES];
EXTERN double                     Jwght[MAX_SPECIES];
EXTERN double                     Awght[MAX_SPECIES];
#else
#undef QNA
#define QNA                       0
#endif

#if (JFRAC == 1)
#if (JFRACFIXED != 1)
#undef JFRACFIXED
#define JFRACFIXED                0                                                 // Fixed juvenile fraction only iff -DJFRACFIXED=1 
#endif
#endif

#ifndef BASALSPECIES
#define BASALSPECIES              1
#endif

#ifndef TRANSIENT_TIME
#define TRANSIENT_TIME            1.0E3                                             // Default: 1.0E4
#endif

#ifndef MIN_PERSIST_TIME
#define MIN_PERSIST_TIME          1.0E5                                             // Default: 1.0E6
#endif

#if (JFRAC == 0)
#define SPECIESDBLLEN             (5*STAGES + 16)                                   // length of species data vector with doubles
#else

/*
 * When dynamics in terms of total density (JFRAC == 1) add:
 * 
 * SPECIESDBLLEN - 3 : Current juvenile fraction
 * SPECIESDBLLEN - 2 : Current total density
 * SPECIESDBLLEN - 1 : Initial juvenile fraction
 * 
 * The ODE system that is integrated starts at SPECIESDBLLEN - 3
 */

#define SPECIESDBLLEN             (5*STAGES + 19)                                   // Add 3 doubles for current and initial fraction and total density
#endif
#define SPECIESINTLEN             4                                                 // length of species data vector with ints
#define SPECIESINPUTLEN           (STAGES + 13)                                     // length of the vector with species input variables

#define ODE_VECTOR_COPIES         11

//==================================================================================
/*
 * Definition of parameter aliases
 */

#define PARAMETER_NR              27

#define PRODUCTIVITY              parameter[ 0]                                     // Productivity basal species
#define TURNOVER                  parameter[ 1]                                     // Turn-over rate basal species

#define ALPHA_CONST               parameter[ 2]                                     // Scaling constant of Alpha
#define ALPHA_WIDTH               parameter[ 3]                                     // One-sided width of this constant

#define FHRES_CONST               parameter[ 4]                                     // Half-saturation constant in func. resp.
#define FHRES_WIDTH               parameter[ 5]                                     // One-sided width of this constant

#define JUVADUSPEC_CONST          parameter[ 6]                                     // Constant of juv./adult specilisation
#define JUVADUSPEC_WIDTH          parameter[ 7]                                     // One-sided width of this constant

#define GAMMA_CONST               parameter[ 8]                                     // Scaling constant of Gamma
#define GAMMA_WIDTH               parameter[ 9]                                     // One-sided width of this constant

#define MAINT_CONST               parameter[10]                                     // Scaling constant of Maint
#define MAINT_WIDTH               parameter[11]                                     // One-sided width of this constant

#define MORT_CONST                parameter[12]                                     // Scaling constant of Mort
#define MORT_WIDTH                parameter[13]                                     // One-sided width of this constant

#define QALPHA                    parameter[14]                                     // Adult-juvenile difference in Alpha (fixed)
#define QMAINT                    parameter[15]                                     // Adult-juvenile difference in Maint (fixed)
#define QGAMMA                    parameter[16]                                     // Adult-juvenile difference in Gamma (fixed)
#define QMORT                     parameter[17]                                     // Adult-juvenile difference in Mort  (fixed)

#define MED_LOGPPMR_MIN           parameter[18]                                     // Lowest median Log10(PPMR)
#define MED_LOGPPMR_MAX           parameter[19]                                     // Highest median Log10(PPMR)
#define LOGPPMR_WIDTH             parameter[20]                                     // One-sided width of the Log10(PPMR) feeding range

#define JUVADUSIZERATIO           parameter[21]                                     // Juvenile-adult body size ratio

#define UNIDEVSEED                parameter[22]                                     // Seed value random generator
#define MIN_SIZE                  parameter[23]                                     // Minimum body size
#define MAX_SIZE                  parameter[24]                                     // Maximum body size
#define INTRO_DENSITY             parameter[25]                                     // Introduction density
#define EXTINCT                   parameter[26]                                     // Extinction density

#define MIN_MORT                  0.001

//==================================================================================
/*
 * Definition of global variables
 */

EXTERN double                     CurrentTime;
EXTERN int                        DynamicsRun;

EXTERN double                     Accuracy;
EXTERN double                     IdenticalZero;
EXTERN double                     MaxTime;
EXTERN double                     OutputInterval;

EXTERN long unsigned              UniDevSeed;

EXTERN int                        SpeciesIntData[SPECIESINTLEN*MAX_SPECIES];
EXTERN double                     SpeciesDblData[SPECIESDBLLEN*MAX_SPECIES];

EXTERN double                     parameter[PARAMETER_NR];

EXTERN int                        *SpeciesID;
EXTERN int                        *SpeciesCol;
EXTERN int                        *PreyNr;
EXTERN int                        *PredNr;

EXTERN double                     *TotDens;
EXTERN double                     *SpeciesAge;

EXTERN double                     *JuvDens;
EXTERN double                     *ImmDens;
EXTERN double                     *AduDens;

EXTERN double                     *NicheVal;
EXTERN double                     *SpeciesBodySize;

EXTERN double                     *Alpha;
EXTERN double                     *Fhres;
EXTERN double                     *JuvAduSpec;
EXTERN double                     *Gamma;
EXTERN double                     *Maint;
EXTERN double                     *Mort;

EXTERN double                     *MinFoodNiche;
EXTERN double                     *MaxFoodNiche;

EXTERN double                     *JuvDensMean;
EXTERN double                     *JuvDensVariance;
EXTERN double                     *JuvDensMin;
EXTERN double                     *JuvDensMax;

EXTERN double                     *ImmDensMean;
EXTERN double                     *ImmDensVariance;
EXTERN double                     *ImmDensMin;
EXTERN double                     *ImmDensMax;

EXTERN double                     *AduDensMean;
EXTERN double                     *AduDensVariance;
EXTERN double                     *AduDensMin;
EXTERN double                     *AduDensMax;

EXTERN double                     *TotDensMean;
EXTERN double                     *TotDensVariance;
EXTERN double                     *TotDensMin;
EXTERN double                     *TotDensMax;

EXTERN double                     ODEMemPnt[ODE_VECTOR_COPIES*STAGES*MAX_SPECIES];

EXTERN int                        Species;
EXTERN int                        InitialSpecies;
EXTERN int                        InitialBU;
EXTERN int                        InitialLinks;

EXTERN double                     Phi[MAX_SPECIES][MAX_SPECIES];
EXTERN int                        prey_index[MAX_SPECIES][MAX_SPECIES];

EXTERN char	                      progname[MAXPATHLEN];                             // Name of the executable
EXTERN char	                      runname[MAXPATHLEN];                              // Name of the current run
EXTERN char	                      usernotes[REPORTNOTE_MAXNR][REPORTNOTE_MAXLEN];		// User defined notes

EXTERN struct dscrptn {				                                                      // Structure with descriptions of quantities in .CVF file
		char Accuracy[DESCRIP_MAX];
		char IdenticalZero[DESCRIP_MAX];
		char MaxTime[DESCRIP_MAX];
		char OutputInterval[DESCRIP_MAX];
		char parameter[PARAMETER_NR][DESCRIP_MAX];
		} description;

//==================================================================================
/*
 * Definition of global macros
 */

#define dmax(a, b)                (((a) > (b)) ? (a) : (b))
#define dmin(a, b)                (((a) < (b)) ? (a) : (b))

#define iszero(a)                 (fabs((a)) < IdenticalZero)

//==================================================================================
/*
 * Declaration of global functions
 */

void                              ConstructInteractionMatrix();
int                               RearrangeMem(int oldSpecies, int newSpecies);
void                              BuildWeb();
void	                            ProgramExit(int exitcode, char *mes);
void	                            PrettyPrint(FILE *fp, double value);
void                              ReportNote(const char *, ...);
void	                            Warning(char *mes);
void                              WriteReport(int argc, char **argv);
void	                            CatchSig(int signl);
void                              Initialize(int argc, char **argv);
double                            UniDev(void);
void                              WebToCsbFile();
void                              odeint(double *ystart, int vecdim, double *xinit, double xmax,
                                         double eps, double hmin, double hmax,
                                         void (*derivs)(double, double *, double *));

//==================================================================================
